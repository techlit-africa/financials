import { defineConfig } from 'vite'
import preact from '@preact/preset-vite'
import AutoImportPlugin from "unplugin-auto-import/vite";
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    AutoImportPlugin({
      imports: [
        "react",
        {
          "react-dom/client": ["createRoot"],
          "react": ["StrictMode"],
        },
      ],
    }),
    preact({
      babel: {
        plugins: [
          'transform-react-pug',
          'transform-jsx-classname-components',
        ],
      },
    }),
  ],

  server: {
    port: 5000,
  },

  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
})
