import {Report, createReport, col, row, t, rowSum} from '@/comps/Report'
import {
  ke2023,
  getKe2023Period,
  nextKe2023Period,
  prevKe2023Period,
} from '@/journals/ke2023'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {$nav, $disabled, $active} from '@/comps/Macros'
import {titleize, date} from '@/lib'

const ke2023PettyCash = (people, period) => {
  const peopleByName = {}
  const peopleNamesByKind = {}

  people.forEach((person) => {
    if (person.kind == 'vendors') return

    if (!peopleNamesByKind[person.kind]) peopleNamesByKind[person.kind] = []
    peopleNamesByKind[person.kind].push(person.name)
    peopleByName[person.name] = person
  })
  const kinds = Object.keys(peopleNamesByKind)

  return createReport({
    journalSignal: ke2023,

    rows: [
      row('summary', 'Summary', [
          ...kinds.map((kind) => row(kind, titleize(kind))),
          row('other', 'Other'),
        ], 'Total'),

      ...kinds.map((kind) =>
        row(`${kind}-breakdown`, titleize(kind), [
          ...peopleNamesByKind[kind].map((name) => row(name, name)),
        ]),
      ),
    ],

    rowFilter: (cells, row) =>
      period == '2023' ||
      row.id == 'summary' ||
      row.id?.endsWith('-breakdown') ||
      cells['opening-advance'][row.id] != 0 ||
      cells['opening-reimbursable'][row.id] != 0 ||
      cells['closing-advance'][row.id] != 0 ||
      cells['closing-reimbursable'][row.id] != 0 ||
      cells['expensed'][row.id] != 0 ||
      cells['reimbursed'][row.id] != 0,

    cols: [
      col(
        'opening',
        [t('Opening', 'center')],
        [
          col('opening-advance', [t('Advance', 'center')]),
          col('opening-reimbursable', [t('Reimbursable', 'center')]),
        ],
      ),
      col(
        'activity',
        [t('Activity', 'center')],
        [
          {...col('expensed', [t('Expensed', 'center')]), type: 'change'},
          {...col('reimbursed', [t('Reimbursed', 'center')]), type: 'change'},
        ],
      ),
      col(
        'closing',
        [t('Closing', 'center')],
        [
          col('closing-advance', [t('Advance', 'center')]),
          col('closing-reimbursable', [t('Reimbursable', 'center')]),
        ],
      ),
    ],

    mapData: ({lines, cells, accountsLookup}, {date, acc, people, dr, cr}) => {
      if (date > period.end) return
      if (acc != 'employee-advance' && acc != 'expenses-reimbursable') return
      if (!people) people = 'other'

      if (acc == 'employee-advance') cells['closing-advance'][people] += dr - cr
      if (acc == 'expenses-reimbursable')
        cells['closing-reimbursable'][people] += cr - dr

      if (date < period.start) {
        if (acc == 'employee-advance')
          cells['opening-advance'][people] += dr - cr
        if (acc == 'expenses-reimbursable')
          cells['opening-reimbursable'][people] += cr - dr
      } else {
        if (cr) cells['expensed'][people] -= cr
        if (dr) cells['reimbursed'][people] += dr
      }
    },

    rowSums: {
      ...kinds.reduce((o, kind) => {
        o[kind] = rowSum(...peopleNamesByKind[kind])
        return o
      }, {}),

      summary: rowSum('other', ...kinds),
    },
  })
}

export const Ke2023PettyCash = ({periodId}) => pug`
  - const period = getKe2023Period(periodId)
  - const nextPeriodId = nextKe2023Period(period.id)
  - const prevPeriodId = prevKe2023Period(period.id)

  Report(report=ke2023PettyCash(ke2023.value.people, period))
    .text-3xl.font-bold Cash / Expenses
    .row.gap-x-4.flex-wrap
      = $nav('/ke2023/stipends/'+periodId, 'Stipends')
      = $active('/ke2023/petty-cash/'+periodId, 'Cash / Expenses')
      = $nav('/ke2023/vendor-payables/'+periodId, 'Vendors')
      = $nav('/ke2023/rent-payables/'+periodId, 'Rent')
      = $nav('/ke2023/school-receivables/'+periodId, 'Schools')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2023/petty-cash/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2023/petty-cash/'+nextPeriodId, 'Next')
`
export default Ke2023PettyCash
