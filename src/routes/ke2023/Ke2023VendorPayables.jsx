import {Report, createReport, col, row, t, rowSum} from '@/comps/Report'
import {
  ke2023,
  getKe2023Period,
  nextKe2023Period,
  prevKe2023Period,
} from '@/journals/ke2023'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {$nav, $disabled, $active} from '@/comps/Macros'
import {titleize, date} from '@/lib'

const ke2023VendorPayables = (people, period) => {
  const vendors = []
  people.forEach((p) => {
    if (p.kind == 'vendors') vendors.push(p)
  })

  return createReport({
    journalSignal: ke2023,

    rowFilter: (cells, row) =>
      period == '2023' ||
        cells['opening'][row.id] != 0 ||
        cells['paid'][row.id] != 0 ||
        cells['billed'][row.id] != 0 ||
        cells['closing'][row.id] != 0,

    rows: [
      row(
        'vendors',
        'Vendors',
        [...vendors.map(({name}) => row(name, name))],
        'Total',
      ),
    ],

    cols: [
      col(
        'summary',
        [],
        [
          col('opening', [t('Opening', 'h-28 font-bold center')]),
          col('paid', [t('Paid', 'h-28 font-bold center')]),
          col('billed', [t('Billed', 'h-28 font-bold center')]),
        ],
      ),

      col('closing', [], [], [t('Closing', 'font-bold')]),
    ],

    mapData: ({lines, cells, accountsLookup}, {date, acc, people, dr, cr}) => {
      if (date > period.end) return
      if (!people || acc != 'vendor-payables') return

      cells['closing'][people] += cr - dr

      if (date < period.start) {
        cells['opening'][people] += cr - dr
      } else {
        if (dr) cells['paid'][people] -= dr
        if (cr) cells['billed'][people] += cr
      }
    },
  })
}

export const Ke2023VendorPayables = ({periodId}) => pug`
  - const period = getKe2023Period(periodId)
  - const nextPeriodId = nextKe2023Period(period.id)
  - const prevPeriodId = prevKe2023Period(period.id)

  Report(report=ke2023VendorPayables(ke2023.value.people, period))
    .text-3xl.font-bold Vendor Payables
    .row.gap-x-4.flex-wrap
      = $nav('/ke2023/stipends/'+periodId, 'Stipends')
      = $nav('/ke2023/petty-cash/'+periodId, 'Cash / Expenses')
      = $active('/ke2023/vendor-payables/'+periodId, 'Vendors')
      = $nav('/ke2023/rent-payables/'+periodId, 'Rent')
      = $nav('/ke2023/school-receivables/'+periodId, 'Schools')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2023/vendor-payables/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2023/vendor-payables/'+nextPeriodId, 'Next')
`
export default Ke2023VendorPayables
