import {
  Report,
  row,
  accRow,
  col,
  t,
  createReport,
  rowSum,
  colSum,
} from '@/comps/Report'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {$active, $nav} from '@/comps/Macros'
import {date} from '@/lib'

import {ke2023, ke2023Periods, dateToKe2023Period} from '@/journals/ke2023'

const rowIdForAccountName = (name) =>
  ({
    'phone-and-data-expense': 'other-expense',
    'uncollectible-expense': 'other-expense',
    'marketing-expense': 'other-expense',
  })[name] || name

const ke2023ActivityByWeek = () => {
  const weeks = ke2023Periods.filter(({id}) => id != '2023')
  // ({end}) => end <= ke2023.value.settings.latest,

  return createReport({
    journalSignal: ke2023,

    rows: [
      row(
        'net',
        'Activity',
        [
          row(
            'rev',
            'Revenue',
            [
              accRow('school-fee-revenue', 'School Fees'),
              // accRow('scholarship-revenue', 'Scholarships'),
              // accRow('cash-donation-revenue', 'Donations'),
              accRow('other-revenue', 'Donations & Other'),
            ],
            'Total Revenue',
          ),

          row(
            'exp',
            'Expenses',
            [
              accRow('stipends-expense', 'Stipends'),
              accRow('transport-expense', 'Transportation'),
              accRow('installation-expense', 'Installation'),
              accRow('supplies-expense', 'Supplies'),
              accRow('rent-expense', 'Rent'),
              accRow('electricity-expense', 'Electricity'),
              accRow('food-expense', 'Food'),
              accRow('depreciation-expense', 'Depreciation'),
              // accRow('uncollectible-expense', 'Uncollectible Revenue'),
              accRow('money-expense', 'Money Fees'),
              // accRow('phone-and-data-expense', 'Phone & Data'),
              // accRow('marketing-expense', 'Marketing'),
              accRow('other-expense', 'Other'),
            ],
            'Total Expenses',
          ),
        ],
        'Net Surplus (Deficit)',
      ),

      // row('summary', 'Summary', [
      //   {
      //     ...row(
      //       'week-margin',
      //       'Weekly',
      //       [
      //         row('week-rev', 'Revenue'),
      //         row('week-exp', 'Expenses'),
      //         row('week-net', 'Surplus (Deficit)'),
      //       ],
      //       'Margin',
      //     ),
      //     type: 'percent',
      //   },
      //
      //   {
      //     ...row(
      //       '4-week-margin',
      //       'Rolling (4w)',
      //       [
      //         row('4-week-rev', 'Revenue'),
      //         row('4-week-exp', 'Expenses'),
      //         row('4-week-net', 'Surplus (Deficit)'),
      //       ],
      //       'Rolling Margin',
      //     ),
      //     type: 'percent',
      //   },
      // ]),
    ],

    cols: [
      col(
        'prev',
        [],
        [
          col('2022', [
            t('Dec 31', 'text-sm opacity-50'),
            t('2022', 'text-lg font-bold'),
          ]),
        ],
      ),
      col(
        'curr',
        [],
        [
          col('2023', [
            t('Dec 31', 'text-sm opacity-50'),
            t('2023', 'text-lg font-bold'),
          ]),
        ],
      ),
      col(
        'weeks',
        [],
        [
          ...ke2023Periods
            .filter(({id}) => id != '2023')
            // .filter(({end}) => end <= ke2023.value.settings.latest)
            .map(({id, start, end}) =>
              col(id, [
                t(
                  `${date(start)} -`,
                  'h-4 text-sm text-gray-400 justify-center',
                ),
                t(date(end), 'justify-end'),
              ]),
            ),
        ],
      ),
    ],

    initData: ({cells}) => {
      cells['2022']['school-fee-revenue'] = 469750 + 240000
      // cells['2022']['scholarship-revenue'] = 240000
      // cells['2022']['cash-donation-revenue'] = 2702706
      cells['2022']['other-revenue'] = 10 + 2702706
      cells['2022']['rev'] = 0

      cells['2022']['stipends-expense'] = 1296475
      cells['2022']['transport-expense'] = 581862
      cells['2022']['installation-expense'] = 78200
      cells['2022']['electricity-expense'] = 136074
      cells['2022']['supplies-expense'] = 43850
      cells['2022']['food-expense'] = 50187
      cells['2022']['rent-expense'] = 151500
      cells['2022']['depreciation-expense'] = 58032
      // cells['2022']['uncollectible-expense'] = 0
      cells['2022']['money-expense'] = 16593
      cells['2022']['phone-and-data-expense'] = 107532
      cells['2022']['marketing-expense'] = 0
      cells['2022']['other-expense'] = 41600 + 229637
      cells['2022']['exp'] = 0

      cells['2022']['net'] = 0

      //   cells['2022']['week-rev'] = 0
      //   cells['2022']['week-exp'] = 0
      //   cells['2022']['week-net'] = 0
      //   cells['2022']['week-margin'] = 0
    },

    mapData: ({cells, accountsLookup}, {date, acc, people, dr, cr}) => {
      if (date < '2023-01-01') return

      const rowId = rowIdForAccountName(acc)
      const colId = dateToKe2023Period(date)
      const {nat_dr} = accountsLookup[acc]
      cells[colId][rowId] += nat_dr ? dr - cr : cr - dr
      cells['2023'][rowId] += nat_dr ? dr - cr : cr - dr
    },

    rowSums: {
      net: ({cells}, colId, rowId) => {
        const col = cells[colId]
        col[rowId] = col['rev'] - col['exp']
      },

      // 'week-rev': rowSum('rev'),
      // 'week-exp': rowSum('exp'),
      // 'week-net': rowSum('net'),
      // 'week-margin': ({cells}, colId, rowId) => {
      //   const col = cells[colId]
      //   col[rowId] = col['net'] / col['rev']
      // },

      // '4-week-rev': ({cells}, colId, rowId) => {
      //   if (
      //     colId == 'weeks' ||
      //     colId == '2023' ||
      //     colId == 'prev' ||
      //     colId == '2022' ||
      //     colId < 3
      //   )
      //     return
      //   cells[colId][rowId] =
      //     (cells[colId - 3]['rev'] +
      //       cells[colId - 2]['rev'] +
      //       cells[colId - 1]['rev'] +
      //       cells[colId]['rev']) /
      //     4
      // },
      // '4-week-exp': ({cells}, colId, rowId) => {
      //   if (
      //     colId == 'weeks' ||
      //     colId == '2023' ||
      //     colId == 'prev' ||
      //     colId == '2022' ||
      //     colId < 3
      //   )
      //     return
      //   cells[colId][rowId] =
      //     (cells[colId - 3]['exp'] +
      //       cells[colId - 2]['exp'] +
      //       cells[colId - 1]['exp'] +
      //       cells[colId]['exp']) /
      //     4
      // },
      // '4-week-net': ({cells}, colId, rowId) => {
      //   if (
      //     colId == 'weeks' ||
      //     colId == '2023' ||
      //     colId == 'prev' ||
      //     colId == '2022' ||
      //     colId < 3
      //   )
      //     return
      //   cells[colId][rowId] =
      //     cells[colId]['4-week-rev'] - cells[colId]['4-week-exp']
      // },
      // '4-week-margin': ({cells}, colId, rowId) => {
      //   if (
      //     colId == 'weeks' ||
      //     colId == '2023' ||
      //     colId == 'prev' ||
      //     colId == '2022' ||
      //     colId < 3
      //   )
      //     return
      //   cells[colId][rowId] =
      //     cells[colId]['4-week-net'] / cells[colId]['4-week-rev']
      // },
    },
  })
}

export const Ke2023ActivityByWeek = () => pug`
  Report(report=ke2023ActivityByWeek())
    .text-3xl.font-bold Statement of Activity
    .row.gap-4
      = $active('/ke2023/activity-by-week', 'Weekly')
      = $nav('/ke2023/activity-by-school/latest', 'Schools')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/ke2023/position-by-week') (Position)
`
export default Ke2023ActivityByWeek
