import {Report, row, accRow, col, t, createReport, colSum, rowSum} from '@/comps/Report'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {titleize, date} from '@/lib'

import {
  ke2023,
  ke2023Periods,
  getKe2023Period,
  nextKe2023Period,
  prevKe2023Period,
  dateToKe2023Period,
} from '@/journals/ke2023'

const ke2023RentPayables = (allSchools, period) => {
  const schools = []

  allSchools.forEach((school) => {
    if (school.tier == '0') return
    schools.push(school.name)
  })

  return createReport({
    journalSignal: ke2023,

    rows: [
      col(
        'summary',
        'Summary',
        [...schools.map((name) => col(name, titleize(name)))],
        'Total',
      ),
    ],

    rowFilter: (cells, row) =>
      period == '2023' ||
        row.id == 'summary' ||
        cells['opening'][row.id] != 0 ||
        cells['paid'][row.id] != 0 ||
        cells['expensed'][row.id] != 0 ||
        cells['closing'][row.id] != 0,

    cols: [
      col(
        'summary',
        [],
        [
          col('opening', [t('Opening', 'h-28 font-bold center')]),
          {
            ...col('paid', [t('Paid', 'h-28 font-bold center')]),
            type: 'change',
          },
          {
            ...col('expensed', [t('Expensed', 'h-28 font-bold center')]),
            type: 'change',
          },
        ],
      ),

      col('closing', [], [], [t('Closing', 'font-bold')]),
    ],

    mapData: ({cells, accountsLookup}, {date, school, acc, dr, cr}) => {
      if (date > period.end) return
      if (!school || acc != 'rent-expense') return

      cells['closing'][school] += dr - cr

      if (date < period.start) {
        cells['opening'][school] += dr - cr
      } else {
        if (dr) cells['paid'][school] += dr
        if (cr) cells['expensed'][school] -= cr
      }
    },

    rowSums: {
      summary: rowSum(...schools),
    },
  })
}

export const Ke2023RentPayables = ({periodId}) => pug`
  - const period = getKe2023Period(periodId)
  - const nextPeriodId = nextKe2023Period(period.id)
  - const prevPeriodId = prevKe2023Period(period.id)

  Report(report=ke2023RentPayables(ke2023.value.schools, period))
    .text-3xl.font-bold Rent Payables
    .row.gap-x-4.flex-wrap
      = $nav('/ke2023/stipends/'+periodId, 'Stipends')
      = $nav('/ke2023/petty-cash/'+periodId, 'Cash / Expenses')
      = $nav('/ke2023/vendor-payables/'+periodId, 'Vendors')
      = $active('/ke2023/rent-payables/'+periodId, 'Rent')
      = $nav('/ke2023/school-receivables/'+periodId, 'Schools')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2023/rent-payables/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2023/rent-payables/'+nextPeriodId, 'Next')
`
export default Ke2023RentPayables
