import {
  Report,
  row,
  accRow,
  col,
  t,
  createReport,
  colSum,
  rowSum,
} from '@/comps/Report'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {titleize, date} from '@/lib'

import {
  ke2023,
  ke2023Periods,
  getKe2023Period,
  nextKe2023Period,
  prevKe2023Period,
  dateToKe2023Period,
} from '@/journals/ke2023'

const rowIdForAccountName = (name) =>
  ({
    'phone-and-data-expense': 'other-expense',
    'uncollectible-expense': 'other-expense',
    'marketing-expense': 'other-expense',
  })[name] || name

const ke2023ActivityBySchool = (schools, period) => {
  const schoolByName = {}
  const schoolNamesByKind = {}

  schools.forEach((school) => {
    if (school.tier == '0') return

    if (!schoolNamesByKind[school.kind]) schoolNamesByKind[school.kind] = []
    schoolNamesByKind[school.kind].push(school.name)
    schoolByName[school.name] = school
  })
  const kinds = Object.keys(schoolNamesByKind)

  return createReport({
    journalSignal: ke2023,

    rows: [
      row(
        'net',
        'Activity',
        [
          row(
            'rev',
            'Revenue',
            [
              accRow('school-fee-revenue', 'School Fees'),
              // accRow('scholarship-revenue', 'Scholarships'),
              // accRow('cash-donation-revenue', 'Donations'),
              accRow('other-revenue', 'Other'),
            ],
            'Total Revenue',
          ),

          row(
            'exp',
            'Expenses',
            [
              accRow('stipends-expense', 'Stipends'),
              accRow('transport-expense', 'Transportation'),
              accRow('installation-expense', 'Installation'),
              accRow('electricity-expense', 'Electricity'),
              accRow('supplies-expense', 'Supplies'),
              accRow('food-expense', 'Food'),
              accRow('rent-expense', 'Rent'),
              accRow('depreciation-expense', 'Depreciation'),
              // accRow('uncollectible-expense', 'Uncollectible Revenue'),
              accRow('money-expense', 'Money Fees'),
              // accRow('phone-and-data-expense', 'Phone & Data'),
              // accRow('marketing-expense', 'Marketing'),
              accRow('other-expense', 'Other'),
            ],
            'Total Expenses',
          ),
        ],
        'Net Surplus (Deficit)',
      ),

      // row('summary', 'Summary', [
      //   {
      //     ...row(
      //       'week-margin',
      //       'Weekly',
      //       [
      //         row('week-rev', 'Revenue'),
      //         row('week-exp', 'Expenses'),
      //         row('week-net', 'Surplus (Deficit)'),
      //       ],
      //       'Margin',
      //     ),
      //     type: 'percent',
      //   },
      // ]),
    ],

    cols: [
      col(
        'all',
        [t('Summary', 'center')],
        [
          col('shared', [t('Shared', 'font-bold center')]),
          col('schools', [t('Schools', 'font-bold center')]),
        ],
        [t('Total', 'center')],
      ),

      col(
        'schools-breakdown',
        [t('Schools', 'center')],
        [
          ...kinds.map((kind) =>
            col(kind, [t(titleize(kind), 'font-bold center')]),
          ),

          ...kinds.map((kind) =>
            col(
              `${kind}-breakdown`,
              [t(titleize(kind), 'center')],
              [
                ...schoolNamesByKind[kind].map((name) =>
                  col(name, [t(titleize(name), 'center')]),
                ),
              ],
            ),
          ),
        ],
      ),
    ],

    colFilter: (cells, col) => {
      if (col.id == 'all' || col.id == 'shared') return true
      if (col.id.endsWith('-breakdown')) {
        const summaryId = col.id.replace('-breakdown', '')
        return cells[summaryId]['rev'] + cells[summaryId]['exp'] != 0
      }
      return cells[col.id]['rev'] + cells[col.id]['exp'] != 0
    },

    mapData: ({cells, accountsLookup}, {date, school, acc, dr, cr}) => {
      if (date < period.start || date > period.end) return
      const colId = schoolByName[school]?.name || 'shared'
      const rowId = rowIdForAccountName(acc)
      const {nat_dr} = accountsLookup[acc]
      cells[colId][rowId] += nat_dr ? dr - cr : cr - dr
    },

    colSums: {
      ...kinds.reduce((o, kind) => {
        o[kind] = colSum(...schoolNamesByKind[kind])
        return o
      }, {}),

      schools: colSum(...kinds),
      all: colSum('shared', 'schools'),
    },

    rowSums: {
      net: ({cells}, colId, rowId) => {
        const col = cells[colId]
        col[rowId] = col['rev'] - col['exp']
      },

      // 'week-rev': rowSum('rev'),
      // 'week-exp': rowSum('exp'),
      // 'week-net': rowSum('net'),
      // 'week-margin': ({cells}, colId, rowId) => {
      //   const col = cells[colId]
      //   col[rowId] = col['net'] / col['rev']
      // },
    },
  })
}

export const Ke2023ActivityBySchool = ({periodId}) => pug`
  - const period = getKe2023Period(periodId)
  - const nextPeriodId = nextKe2023Period(period.id)
  - const prevPeriodId = prevKe2023Period(period.id)

  Report(report=ke2023ActivityBySchool(ke2023.value.schools, period))
    .text-3xl.font-bold Statement of Activity
    .row.gap-4
      = $nav('/ke2023/activity-by-week', 'Weekly')
      = $active('/ke2023/activity-by-school/latest', 'Schools')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/ke2023/position-by-week') (Position)
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2023/activity-by-school/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2023/activity-by-school/'+nextPeriodId, 'Next')
`
export default Ke2023ActivityBySchool
