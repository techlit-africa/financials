import {$btn, $nav} from '@/comps/Macros'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'

export const Ke2023Index = () => pug`
  .row.max-w-screen-md.flex-wrap.gap-12.mt-12
    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Statements of Position
      = $nav('/ke2023/position-by-week', 'Weekly Position')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Statements of Activity
      = $nav('/ke2023/activity-by-week', 'Weekly Activity')
      = $nav('/ke2023/activity-by-school/latest', 'School Activity')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Journals & Ledgers
      = $nav('/ke2023/journal', 'General Journal')
      = $nav('/ke2023/accounts', 'Account Ledgers')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Other Reports
      = $nav('/ke2023/stipends/latest', 'Stipends')
      = $nav('/ke2023/petty-cash/latest', 'Cash / Expenses')
      = $nav('/ke2023/vendor-payables/latest', 'Vendors')
      = $nav('/ke2023/rent-payables/latest', 'Rent')
      = $nav('/ke2023/school-receivables/latest', 'Schools')
`
export default Ke2023Index
