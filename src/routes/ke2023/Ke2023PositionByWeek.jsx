import {Report, row, accRow, col, t, createReport} from '@/comps/Report'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {$active, $nav} from '@/comps/Macros'

import {ke2023, ke2023Periods, dateToKe2023Period} from '@/journals/ke2023'

const rowIdForAccountName = (name) =>
  ({
    'general-net-assets': 'unrestricted',

    'mri-net-assets': 'restricted',
    'headquarters-net-assets': 'restricted',
    'media-studio-net-assets': 'restricted',
    'iex-schools-net-assets': 'restricted',
    'logiri-school-net-assets': 'restricted',
    'chepyuan-school-net-assets': 'restricted',
  })[name] || name

const ke2023PositionByWeek = () =>
  createReport({
    journalSignal: ke2023,

    isCumulative: true,

    rows: [
      row(
        'assets',
        'Assets',
        [
          row(
            'cash',
            'Cash',
            [
              accRow('kcb-checking', 'KCB Checking'),
              accRow('employee-advance', 'Employee Advance'),
            ],
            'Total Cash',
          ),

          row(
            'receivables',
            'Receivables',
            [
              accRow('school-fees-receivable', 'School Fees'),
              // accRow('scholarships-receivable', 'Scholarships'),
              // accRow('other-receivables', 'Other'),
            ],
            'Total Receivables',
          ),

          row(
            'fixed',
            'Fixed Assets',
            [
              // accRow('prepaid-rent', 'Prepaid Rent'),
              accRow('motorbike-assets', 'Motorbikes'),
              accRow('furniture-assets', 'Furniture'),
              accRow('equipment-assets', 'Equipment'),
              accRow('media-studio-assets', 'Media Studio'),
            ],
            'Total Fixed Assets',
          ),
        ],
        'Total Assets',
      ),

      row(
        'lna',
        'Liabilities & Net Assets',
        [
          row(
            'liabilities',
            'Liabilities',
            [
              row(
                'prepayments',
                'Prepaid Services',
                [
                  accRow('prepaid-school-fees', 'School Fees'),
                  // accRow('prepaid-scholarships', 'Scholarships'),
                ],
                'Total Prepaid Services',
              ),

              row(
                'payables',
                'Payables',
                [
                  accRow('stipends-payable', 'Stipends Payable'),
                  accRow('expenses-reimbursable', 'Expenses Reimbursable'),
                  accRow('vendor-payables', 'Vendors Payable'),
                  // accRow('other-payables', 'Other'),
                ],
                'Total Payables',
              ),
            ],
            'Total Liabilities',
          ),

          row(
            'net-assets',
            'Net Assets',
            [
              accRow('unrestricted', 'Unrestricted'),
              accRow('restricted', 'Restricted'),
            ],
            'Total Net Assets',
          ),

          {
            ...row('net-assets-change', 'Total Change in Net Assets'),
            type: 'change',
          },
        ],
        'Total Liabilities & Net Assets',
      ),
    ],

    cols: [
      col(
        '2022',
        [],
        [
          col('-1', [
            t('As of', 'h-4 text-sm text-gray-400 justify-start'),
            t('Dec 31, 2022', 'justify-end'),
          ]),
        ],
      ),

      col(
        '2023',
        [],
        [
          ...ke2023Periods
            .filter(({id}) => id != '2023')
            // .filter(({end}) => end <= ke2023.value.settings.latest)
            .map(({id, name}) =>
              col(id, [
                t('As of', 'h-4 text-sm text-gray-400 justify-center'),
                t(name, 'justify-end'),
              ]),
            ),
        ],
      ),

      col(
        'ytd',
        [],
        [],
        [
          t('As of', 'h-4 text-sm text-yellow-800 justify-start'),
          t('Dec 31, 2023', 'justify-end'),
        ],
      ),
    ],

    mapData: ({cells, accountsLookup}, {date, acc, dr, cr}) => {
      if (date > ke2023.value.settings.latest) return
      const colId = dateToKe2023Period(date)
      const rowId = rowIdForAccountName(acc)
      const {nat_dr} = accountsLookup[acc]
      cells[colId][rowId] += nat_dr ? dr - cr : cr - dr
    },

    rowSums: {
      'lna': ({cells}, colId, rowId) => {
        const col = cells[colId]
        col[rowId] = col['liabilities'] + col['net-assets']
      },

      'net-assets-change': ({cells}, colId, rowId) => {
        const curr = cells[colId]

        if (colId == '-1' || colId == '2022' || colId == 'ytd') {
          curr.change = 0
        } else {
          const prev = cells[colId - 1]
          curr[rowId] = curr['net-assets'] - prev['net-assets']
        }
      },
    },
  })

export const Ke2023PositionByWeek = () => pug`
  Report(report=ke2023PositionByWeek())
    .text-3xl.font-bold Statement of Position
    .row.gap-4
      = $active('/ke2023/position-by-week', 'Weekly')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/ke2023/activity-by-week') (Activity)
`
export default Ke2023PositionByWeek
