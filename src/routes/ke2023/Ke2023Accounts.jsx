import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {ke2023} from '@/journals/ke2023'

const $nav = ({name, description}) => pug`
  a.row.items-center.gap-1.text-3xl.font-bold(key=name href='/ke2023/accounts/'+name) [ #[.text-lg.text-blue-700= description || name] ]
`

export const Ke2023Accounts = () => pug`
  - const {accounts} = ke2023.value

  .flex-1.col.items-start.gap-2.p-8.overflow-y-scroll
    h2.inline.px-1.text-2xl.font-semibold.border-b-4.border-yellow-300 Accounts
    for account in accounts || []
      if account.name
        = $nav(account)
`
export default Ke2023Accounts
