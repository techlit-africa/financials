import {Report, createReport, col, row, t, rowSum} from '@/comps/Report'
import {
  ke2023,
  getKe2023Period,
  nextKe2023Period,
  prevKe2023Period,
} from '@/journals/ke2023'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {$nav, $disabled, $active} from '@/comps/Macros'
import {titleize, date} from '@/lib'

const ke2023Stipends = (people, period) => {
  const educators = []
  const other = []
  people.forEach((p) => (p.kind == 'educators' ? educators : other).push(p))

  return createReport({
    journalSignal: ke2023,

    rows: [
      row('summary', 'Summary', [
          row('educators', 'Educators'),
          row('other', 'Other'),
          row('unknown', 'Unknown'),
        ], 'Total'),

      row('educators', 'Educators',
        [...educators.map(({name}) => row(name, name))],
        'Total'),

      row('other', 'Other',
        [...other.map(({name}) => row(name, name))],
        'Total'),
    ],

    rowFilter: (cells, row) =>
      cells['opening'][row.id] != 0 ||
      cells['paid'][row.id] != 0 ||
      cells['closing'][row.id] != 0,

    cols: [
      col('summary', [], [
          col('opening', [t('Opening', 'h-28 font-bold center')]),
          {...col('paid', [t('Paid', 'h-28 font-bold center')]), type: 'change'},
          {...col('earned', [t('Earned', 'h-28 font-bold center')]), type: 'change'},
        ]),
      col('closing', [], [], [t('Closing', 'font-bold')]),
    ],

    mapData: ({lines, cells, accountsLookup}, {date, acc, people, dr, cr}) => {
      if (date > period.end) return
      if (acc != 'stipends-payable') return
      if (!people) people = 'unknown'

      cells['closing'][people] += cr - dr

      if (date < period.start) {
        cells['opening'][people] += cr - dr
      } else {
        if (dr) cells['paid'][people] -= dr
        if (cr) cells['earned'][people] += cr
      }
    },

    rowSums: {summary: rowSum('other', 'educators', 'unknown')},
  })
}

export const Ke2023Stipends = ({periodId}) => pug`
  - const period = getKe2023Period(periodId)
  - const nextPeriodId = nextKe2023Period(period.id)
  - const prevPeriodId = prevKe2023Period(period.id)

  Report(report=ke2023Stipends(ke2023.value.people, period))
    .text-3xl.font-bold Stipends
    .row.gap-x-4.flex-wrap
      = $active('/ke2023/stipends/'+periodId, 'Stipends')
      = $nav('/ke2023/petty-cash/'+periodId, 'Cash / Expenses')
      = $nav('/ke2023/vendor-payables/'+periodId, 'Vendors')
      = $nav('/ke2023/rent-payables/'+periodId, 'Rent')
      = $nav('/ke2023/school-receivables/'+periodId, 'Schools')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2023/stipends/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2023/stipends/'+nextPeriodId, 'Next')
`
export default Ke2023Stipends
