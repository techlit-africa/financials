import {useEffect} from 'preact/hooks'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {ke2023Entries} from '@/journals/ke2023'
import {Entry, Line, $tag} from '@/comps/Journal'

export const Ke2023Journal = () => pug`
  -
    useEffect(() => {
      const hash = window.location.hash
      window.location.hash = ''
      window.location.hash = hash
    }, [ke2023Entries.value])

  .w-full.min-w-screen-md.flex-1.overflow-y-scroll.smooth-scroll
    for entry in ke2023Entries.value
      Entry(key=entry.id entry=entry)
        for line in entry.lines
          Line(key=line.id line=line)
            if line.fund != 'general'
              = $tag('$', line.fund, 'bg-pink-100')
            = $tag('@', line.people, 'bg-blue-100')
            = $tag('#', line.school, 'bg-orange-100')
`
export default Ke2023Journal
