import {
  Report,
  row,
  accRow,
  col,
  t,
  createReport,
  colSum,
  rowSum,
} from '@/comps/Report'
import {Ke2023Layout} from '@/layouts/Ke2023Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {titleize, date} from '@/lib'

import {
  ke2023,
  ke2023Periods,
  getKe2023Period,
  nextKe2023Period,
  prevKe2023Period,
  dateToKe2023Period,
} from '@/journals/ke2023'

const ke2023SchoolReceivables = (schools, period) => {
  const schoolByName = {}
  const schoolNamesByKind = {}

  schools.forEach((school) => {
    if (school.tier == '0') return

    if (!schoolNamesByKind[school.kind]) schoolNamesByKind[school.kind] = []
    schoolNamesByKind[school.kind].push(school.name)
    schoolByName[school.name] = school
  })
  const kinds = Object.keys(schoolNamesByKind)

  return createReport({
    journalSignal: ke2023,

    rows: [
      row('summary', 'Summary',
        schools.map(({name}) => row(name, titleize(name))),
      'Total'),
    ],

    rowFilter: (cells, row) =>
      cells['opening-prepaid'][row.id] != 0 ||
      cells['opening-receivable'][row.id] != 0 ||
      cells['closing-prepaid'][row.id] != 0 ||
      cells['closing-receivable'][row.id] != 0,

    cols: [
      col('opening', [t('Opening', 'center')], [
        col('opening-prepaid', [t('Prepaid', 'center')]),
        col('opening-receivable', [t('Receivable', 'center')]),
      ]),
      col('activity', [t('Activity', 'center')], [
        {...col('billed', [t('Billed', 'center')]), type: 'change'},
        {...col('received', [t('Received', 'center')]), type: 'change'},
      ]),
      col('closing', [t('Closing', 'center')], [
        col('closing-prepaid', [t('Prepaid', 'center')]),
        col('closing-receivable', [t('Receivable', 'center')]),
      ]),
    ],

    mapData: ({cells, accountsLookup}, {date, school, acc, dr, cr}) => {
      if (date > period.end) return
      if (!school) return
      if (acc != 'school-fees-receivable' && acc != 'prepaid-school-fees')
        return

      if (acc == 'prepaid-school-fees')    cells['closing-prepaid'   ][school] -= cr - dr
      if (acc == 'school-fees-receivable') cells['closing-receivable'][school] += dr - cr

      if (date < period.start) {
        if (acc == 'prepaid-school-fees')    cells['opening-prepaid'   ][school] -= cr - dr
        if (acc == 'school-fees-receivable') cells['opening-receivable'][school] += dr - cr
      } else {
        if (dr) cells['billed'  ][school] += dr
        if (cr) cells['received'][school] -= cr
      }
    },
  })
}

export const Ke2023SchoolReceivables = ({periodId}) => pug`
  - const period = getKe2023Period(periodId)
  - const nextPeriodId = nextKe2023Period(period.id)
  - const prevPeriodId = prevKe2023Period(period.id)

  Report(report=ke2023SchoolReceivables(ke2023.value.schools, period))
    .text-3xl.font-bold School Receivables
    .row.gap-x-4.flex-wrap
      = $nav('/ke2023/stipends/'+periodId, 'Stipends')
      = $nav('/ke2023/petty-cash/'+periodId, 'Cash / Expenses')
      = $nav('/ke2023/vendor-payables/'+periodId, 'Vendors')
      = $nav('/ke2023/rent-payables/'+periodId, 'Rent')
      = $active('/ke2023/school-receivables/'+periodId, 'Schools')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2023/school-receivables/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2023/school-receivables/'+nextPeriodId, 'Next')
`
export default Ke2023SchoolReceivables
