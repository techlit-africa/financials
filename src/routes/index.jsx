import {$nav} from '@/comps/Macros'

export const Index = () => pug`
  .col.h-screen.max-w-full.pt-2.overflow-hidden
    .row.px-2.pt-2.gap-4.items-center
      a.inline.text-2xl.font-semibold.hover_text-blue-700.border-b-8.border-yellow-300.hover_border-blue-700(
        href='/ke2023'
      ) TechLit Financials

    .row
      .col.p-4.gap-2
        = $nav('/us2023', "US 2023")
        = $nav('/ke2023', "Kenya 2023")

      .col.p-4.gap-2
        = $nav('/us2024', "US 2024")
        = $nav('/ke2024', "Kenya 2024")
`

export default Index
