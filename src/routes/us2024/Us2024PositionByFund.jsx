import {Report, row, accRow, col, t, createReport, colSum} from '@/comps/Report'
import {Us2024Layout} from '@/layouts/Us2024Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date} from '@/lib'

import {dateToUs2024Period, getUs2024Period, nextUs2024Period, prevUs2024Period, us2024} from '@/journals/us2024'

const rowIdForAccountName = (name) => ({
  'general-net-assets':              'net-assets',

  'admin-net-assets':                'net-assets',
  'kenya-hq-net-assets':             'net-assets',
  'media-studio-net-assets':         'net-assets',
  'iex-schools-net-assets':          'net-assets',
  'logiri-school-net-assets':        'net-assets',
  'chepyuan-school-net-assets':      'net-assets',
  'student-scholarships-net-assets': 'net-assets',
  // 'shipping-net-assets':             'net-assets',
})[name] || name

const colIdForFundName = (name) => ({
  'general': 'unrestricted',
})[name] || name

const us2024PositionByFund = (period) => createReport({
  journalSignal: us2024,

  rows: [
    row('assets', 'Assets', [
      row('cash', 'Cash', [
        accRow('checking-general', 'Checking - General'),
        accRow('checking-admin',   'Checking - Admin'),
        accRow('paypal-cash',      'PayPal Account'),
      ], 'Total Cash'),

      row('receivables', 'Receivables', [
        accRow('grants-receivable',         'Grants Receivable'),
        accRow('stripe-receivables',        'Stripe Receivables'),
        accRow('paypal-giving-receivables', 'PayPal Giving'),
      ], 'Total Receivables'),

      row('fixed', 'Fixed Assets', [
        accRow('equipment-assets',       'Computers & Accessories'),
        accRow('kenya-hq-prepaid-lease', 'Kenya HQ Prepaid Lease'),
      ], 'Total Fixed Assets'),
    ], 'Total Assets'),

    row('lna', 'Liabilities Net Assets', [
      row('liabilities', 'Liabilities', [
        accRow('gusto-payable',             'Accrued Payroll'),
        accRow('techlit-providers-payable', 'TechLit Kenya Payables'),
        accRow('reimbursable-expenses',     'Reimbursable Expenses'),
        // accRow('other-payables',            'Other Payables'),
      ], 'Total Liabilities'),

      row('net-assets', 'Net Assets', [], 'Total Net Assets'),
    ], 'Total Liabilities & Net Assets'),
  ],

  // rowSums: {
  //   'net-assets'
  // },

  cols: [
    col('all', [t('Summary', 'center')], [
      col('unrestricted', [t('Unrestricted', 'items-center text-base')]),
      col('restricted',   [t('Restricted',   'items-center text-base')]),
    ], [t('Total', 'center')]),
  
    col('restricted-breakdown', [t('Restrictions', 'center')], [
      col('admin', [t('Admin', 'center font-bold')]),
      col('schools', [t('Schools', 'center font-bold')]),
      col('other', [t('Other', 'center font-bold')]),
  
      col('schools-breakdown', [t('School Support', 'center')], [
        col('iex',      [t('IEX',      'center')]),
        col('logiri',   [t('Logiri',   'center')]),
        col('chepyuan', [t('Chepyuan', 'center')]),
      ]),
  
      col('other-breakdown', [t('Other Restrictions', 'center')], [
        // col('shipping',             [t('Shipping',     'center')]),
        col('kenya-hq',             [t('Kenya HQ',     'center')]),
        col('media-studio',         [t('Media Studio', 'center')]),
        col('student-scholarships', [t('Students',     'center')]),
      ]),
    ]),
  ],

  colSums: {
    'schools': colSum('iex', 'logiri', 'chepyuan'),
    'other': colSum(
      // 'shipping',
      'kenya-hq',
      'media-studio',
      'student-scholarships',
    ),
    'restricted': colSum('admin', 'schools', 'other'),
    'all': colSum('unrestricted', 'restricted'),
  },

  mapData: ({accountsLookup, cells}, {date, fund, acc, dr, cr}) => {
    if (date > period.end) return
    const {nat_dr} = accountsLookup[acc]
    const colId = colIdForFundName(fund)
    const rowId = rowIdForAccountName(acc)
    cells[colId][rowId] += nat_dr ? dr - cr : cr - dr
  },
})

export const Us2024PositionByFund = ({periodId}) => pug`
  - const period = getUs2024Period(periodId)
  - const prevPeriodId = prevUs2024Period(period.id)
  - const nextPeriodId = nextUs2024Period(period.id)

  Report(report=us2024PositionByFund(period) fmt='usd')
    .text-3xl.font-bold Statement of Position
    .row.flex-wrap.gap-x-4
      = $nav('/us2024/position-by-month', 'Monthly')
      = $active('/us2024/position-by-fund/'+periodId, 'Restricted')
      = $nav('/us2024/cash-balances/'+periodId, 'Cash Position')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/us2024/activity-by-fund/'+periodId) (Activity)
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2024/position-by-fund/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2024/position-by-fund/'+nextPeriodId, 'Next')
`
export default Us2024PositionByFund
