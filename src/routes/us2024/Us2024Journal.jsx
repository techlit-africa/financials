import {useEffect} from 'preact/hooks'
import {Us2024Layout} from '@/layouts/Us2024Layout'
import {us2024Entries} from '@/journals/us2024'
import {Entry, Line, $tag} from '@/comps/Journal'

export const Us2024Journal = () => pug`
  -
    useEffect(() => {
      const hash = window.location.hash
      window.location.hash = ''
      window.location.hash = hash
    }, [us2024Entries.value])

  .w-full.min-w-screen-md.flex-1.overflow-y-scroll.smooth-scroll
    for entry in us2024Entries.value
      Entry(key=entry.id entry=entry fmt='usd')
        for line in entry.lines
          Line(key=line.id line=line fmt='usd')
            if line.fund != 'general'
              = $tag('$', line.fund, 'bg-pink-100')
            = $tag('@', line.party, 'bg-blue-100')
            = $tag('#', line.channel, 'bg-orange-100')
`
export default Us2024Journal
