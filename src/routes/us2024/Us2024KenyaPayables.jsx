import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'
import {Us2024Layout} from '@/layouts/Us2024Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date, titleize} from '@/lib'
import {dateToUs2024Period, getUs2024Period, nextUs2024Period, prevUs2024Period, us2024} from '@/journals/us2024'

const us2024KenyaPayables = (allFunds, period) => {
  const funds = []
  allFunds.forEach(({name, kind}) => {
    funds.push(name)
  })
  console.log(funds)
  
  return createReport({
    journalSignal: us2024,

    rows: [
      row('funds', 'Funds', [
        ...funds.map((name) => row(name, name)),
      ], 'Total'),
    ],
    rowFilter: (cells, row) => (
      row.id == 'all'
        || cells['opening'][row.id] > 0
        || cells['services'][row.id] > 0
        || cells['payments'][row.id] > 0
        || cells['exchange'][row.id] > 0
        || cells['closing'][row.id] > 0
    ),

    cols: [
      col('summary', [], [
        col('opening', [t('Opening', 'h-32 font-bold justify-center'),]),
        {...col('exchange', [t('Exchange', 'h-32 font-bold justify-center'),]), type: 'change'},
        {...col('services', [t('Services', 'h-32 font-bold justify-center'),]), type: 'change'},
        {...col('payments', [t('Payments', 'h-32 font-bold justify-center'),]), type: 'change'},
      ]),
      col('closing', [], [], [t('Closing', 'h-32 font-bold justify-center',)]),
    ],

    mapData: ({accountsLookup, cells}, {date, acc, fund, dr, cr}) => {
      if (date > period.end) return
      if (!fund) return

      if (acc == 'techlit-providers-payable') {
        cells['closing'][fund] += cr - dr
  
        if (date < period.start) {
          cells['opening'][fund] += cr - dr
        } else {
          if (cr) cells['services'][fund] += cr
          if (dr) cells['payments'][fund] -= dr
        }
      } if (acc == 'exchange-gains' && date >= period.start) {
        cells['exchange'][fund] += dr - cr
      }
    },
  })
}

export const Us2024KenyaPayables = ({periodId}) => pug`
  - const period = getUs2024Period(periodId)
  - const prevPeriodId = prevUs2024Period(period.id)
  - const nextPeriodId = nextUs2024Period(period.id)

  Report(report=us2024KenyaPayables(us2024.value.funds, period) fmt='usd')
    .text-3xl.pb-4.font-bold Provider Payables
    .row.gap-x-4.flex-wrap
      = $nav('/us2024/cash-balances/'+periodId, 'Cash Balances')
      = $nav('/us2024/top-channels/'+periodId, 'Top Channels')
      = $nav('/us2024/top-donors/'+periodId, 'Top Donors')
      = $nav('/us2024/salaries/'+periodId, 'Salaries')
      = $nav('/us2024/reimbursements/'+periodId, 'Reimbursements')
      = $active('/us2024/kenya-payables/'+periodId, 'Kenya Payables')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2024/kenya-payables/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2024/kenya-payables/'+nextPeriodId, 'Next')
`
export default Us2024KenyaPayables
