import {Report, row, accRow, col, t, createReport, colSum} from '@/comps/Report'
import {Us2024Layout} from '@/layouts/Us2024Layout'
import {$active, $nav} from '@/comps/Macros'

import {dateToUs2024Period, us2024, us2024Periods} from '@/journals/us2024'

import {date} from '@/lib'

const rowIdForAccountName = (name) =>
  ({
    'techlit-equipment-storage-expenses': 'techlit-other-expenses',

    'fundraising-marketing-expenses': 'fundraising-other-expenses',
    'fundraising-advertising-expenses': 'fundraising-other-expenses',
  })[name] || name

const us2024ActivityByMonth = () =>
  createReport({
    journalSignal: us2024,

    rows: [
      row('activity', 'Monthly Activity', [
        row('rev', 'Revenue', [
          accRow('monetary-donations',  'Monetary Donations'),
          accRow('equipment-donations', 'Equipment Donations'),
          accRow('exchange-gains',      'Exchange Gains (Losses)'),
          accRow('other-revenue',       'Other Revenue'),
        ], 'Total Revenue'),

        row('exp', 'Expenses', [
          row('techlit', 'TechLit Program', [
            accRow('techlit-salaries',                        'Salaries'),
            accRow('techlit-travel-expenses',                 'Travel'),
            accRow('techlit-cloud-services-expenses',         'Cloud Services'),
            accRow('techlit-equipment-shipping-expenses',     'Shipping'),
            accRow('techlit-equipment-depreciation-expenses', 'Depreciation'),
            accRow('techlit-provider-fees-expenses',          'TechLit Kenya Fees'),
            accRow('techlit-provider-setup-expenses',         'TechLit Kenya Setup'),
            accRow('techlit-other-expenses',                  'Other'),
          ], 'TechLit Program Total'),

          row('other', 'Other Programs', [
            accRow('kenya-hq-rent-expenses',       'Kenya HQ Rent'),
            accRow('media-studio-setup-expenses',  'Media Studio'),
            accRow('student-scholarship-expenses', 'Student Support'),
            accRow('other-program-expenses',       'Other'),
          ], 'Other Programs Total'),

          row('mgmt', 'Management', [
            accRow('mgmt-salaries',                'Salaries'),
            accRow('mgmt-payroll-taxes',           'Payroll Taxes'),
            accRow('mgmt-money-fees-expenses',     'Money Fees'),
            accRow('mgmt-admin-services-expenses', 'Admin Services'),
            accRow('mgmt-cloud-services-expenses', 'Cloud Services'),
            accRow('mgmt-other-expenses',          'Other'),
          ], 'Management Total'),

          row('fundraising', 'Fundraising Expenses', [
            accRow('fundraising-salaries',        'Salaries'),
            accRow('fundraising-travel-expenses', 'Travel'),
            accRow('fundraising-events-expenses', 'Events'),
            accRow('fundraising-other-expenses',  'Other'),
          ], 'Fundraising Total'),
        ], 'Total Expenses'),
      ], 'Surplus (Deficit)'),
    ],

    // rowFilter: (cells, row) => cells['2024'][row.id] > 0,

    cols: [
      col('2024', [], [], [t('Total'), t('2024')]),

      col(
        'months',
        [],
        [
          ...us2024Periods
            .filter(({id}) => id != '2024')
            // .filter(({end}) => end <= us2024.value.settings.latest)
            .map(({id, start, end}) =>
              col(id, [
                t(
                  `${date(start)} -`,
                  'h-4 text-sm text-gray-400 justify-center',
                ),
                t(date(end), 'justify-end'),
              ]),
            ),
        ],
      ),
    ],

    mapData: ({cells, accountsLookup}, {date, acc, dr, cr}) => {
      const colId = dateToUs2024Period(date)
      if (colId == '2022-12') return

      const rowId = rowIdForAccountName(acc)
      const {nat_dr} = accountsLookup[acc]
      cells[colId][rowId] += nat_dr ? dr - cr : cr - dr
    },

    rowSums: {
      activity: ({cells}, colId, rowId) => {
        const col = cells[colId]
        col[rowId] = col['rev'] - col['exp']
      },
    },

    colSums: {
      2024: colSum(
        ...us2024Periods
          .filter(({end}) => end <= us2024.value.settings.latest)
          .map(({id}) => id),
      ),
    },
  })

export const Us2024ActivityByMonth = () => pug`
  Report(report=us2024ActivityByMonth() fmt='usd')
    .h-full.col
      .text-3xl.font-bold Statement of Activity
      .row.gap-4
        = $active('/us2024/activity-by-month', 'Monthly')
        = $nav('/us2024/activity-by-fund/latest', 'Restricted')
        a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/us2024/position-by-month') (Position)
`
export default Us2024ActivityByMonth
