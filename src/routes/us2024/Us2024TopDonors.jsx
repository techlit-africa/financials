import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'
import {Us2024Layout} from '@/layouts/Us2024Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date, titleize} from '@/lib'
import {dateToUs2024Period, getUs2024Period, nextUs2024Period, prevUs2024Period, us2024} from '@/journals/us2024'

const us2024TopDonors = (parties, period) => {
  let kinds = {}
  const kindByName = {}
  const donors = []
  parties.forEach(({name, kind}) => {
    if (kind == 'employee') return
    donors.push(name)
    kinds[kind] = true
    kindByName[name] = kind
  })
  kinds = Object.keys(kinds)
  
  return createReport({
    journalSignal: us2024,

    rows: [
      row('total', 'Summary', [
        ...kinds.map((kind) => row(kind, titleize(`${kind} Donations`))),
      ], 'Total Donations'),

      row('all', 'All Donors', [
        ...donors.map((name) => row(name, name)),
      ]),
    ],

    rowSorter: (cells, a, b) => cells['closing'][b.id] - cells['closing'][a.id],

    rowFilter: (cells, row) => row.id == 'all' || cells['closing'][row.id] > 0,

    cols: [
      col('all', [], [
        {type: 'change', ...col('current', [t(`${date(period.start)} - ${date(period.end)}`, 'center')], [
          {type: 'change', ...col('monetary-donations', [t('Monetary', 'font-bold center')])},
          {type: 'change', ...col('equipment-donations', [t('Equipment', 'font-bold center')])},
        ], [t('Total', 'font-bold center')])},
  
        col('closing', [t(`2024 Total`, 'center')], [
          col('closing-monetary-donations', [t('Monetary', 'font-bold center')]),
          col('closing-equipment-donations', [t('Equipment', 'font-bold center')]),
        ], [t('Total', 'font-bold center')]),
      ])
    ],

    mapData: ({accountsLookup, cells}, {date, acc, party, dr, cr}) => {
      if (date > period.end) return
      if (!party || (acc != 'monetary-donations' && acc != 'equipment-donations')) return

      const kind = kindByName[party]
      if (!kind || kind == 'employee') return

      cells[`closing-${acc}`][party] += cr - dr
      cells[`closing-${acc}`][kind] += cr - dr

      if (date >= period.start) {
        cells[acc][party] += cr - dr
        cells[acc][kind] += cr - dr
      }
    },
  })
}

export const Us2024TopDonors = ({periodId}) => pug`
  - const period = getUs2024Period(periodId)
  - const prevPeriodId = prevUs2024Period(period.id)
  - const nextPeriodId = nextUs2024Period(period.id)

  Report(report=us2024TopDonors(us2024.value.parties, period) fmt='usd')
    .text-3xl.pb-4.font-bold Top Donors
    .row.gap-x-4.flex-wrap
      = $nav('/us2024/cash-balances/'+periodId, 'Cash Balances')
      = $nav('/us2024/top-channels/'+periodId, 'Top Channels')
      = $active('/us2024/top-donors/'+periodId, 'Top Donors')
      = $nav('/us2024/salaries/'+periodId, 'Salaries')
      = $nav('/us2024/reimbursements/'+periodId, 'Reimbursements')
      = $nav('/us2024/kenya-payables/'+periodId, 'Kenya Payables')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2024/top-donors/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2024/top-donors/'+nextPeriodId, 'Next')
`
export default Us2024TopDonors
