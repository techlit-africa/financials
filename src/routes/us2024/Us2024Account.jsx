import {Us2024Layout} from '@/layouts/Us2024Layout'
import {us2024Accounts} from '@/journals/us2024'
import {Account, Line, $tag} from '@/comps/Account'
import {Failed, Loading} from '@/comps/Suspense'

export const Us2024Account = ({name}) => pug`
  - const account = us2024Accounts.value[name]

  .flex-1.col.gap-2.overflow-y-scroll
    unless account
      Loading

    else
      Account(account=account fmt='usd')
        for line in account.lines
          Line(key=line.id line=line fmt='usd')
            if line.fund != 'general'
              = $tag('$', line.fund, 'bg-pink-100')
            = $tag('@', line.party, 'bg-blue-100')
            = $tag('#', line.channel, 'bg-orange-100')
`
export default Us2024Account
