import {Us2024Layout} from '@/layouts/Us2024Layout'
import {us2024} from '@/journals/us2024'

const $nav = ({name, description}) => pug`
  a.row.items-center.gap-1.text-3xl.font-bold(key=name href='/us2024/accounts/'+name) [ #[.text-lg.text-blue-700= description || name] ]
`

export const Us2024Accounts = () => pug`
  - const {accounts} = us2024.value

  .flex-1.col.items-start.gap-2.p-8.overflow-y-scroll
    h2.inline.px-1.text-2xl.font-semibold.border-b-4.border-yellow-300 Accounts
    for account in accounts || []
      if account.name
        = $nav(account)
`
export default Us2024Accounts
