import {Us2024Layout} from '@/layouts/Us2024Layout'
import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'

import {us2024, us2024Periods, prevUs2024Period, dateToUs2024Period} from '@/journals/us2024'
import {$active, $nav} from '@/comps/Macros'

//
// Rows that have an ID different from their name
//
const rowIdForAccountName = (name) =>
  ({
    'general-net-assets': 'unrestricted',

    'iex-schools-net-assets': 'schools-net-assets',
    'chepyuan-school-net-assets': 'schools-net-assets',
    'logiri-school-net-assets': 'schools-net-assets',

    'shipping-net-assets': 'other-net-assets',
    'kenya-hq-net-assets': 'other-net-assets',
    'media-studio-net-assets': 'other-net-assets',
    'student-scholarships-net-assets': 'other-net-assets',
  })[name] || name

const us2024PositionByMonth = () =>
  createReport({
    journalSignal: us2024,

    isCumulative: true,

    rows: [
      row('assets', 'Assets', [
        row('cash', 'Cash', [
          accRow('checking-general', 'Checking - General'),
          accRow('checking-admin',   'Checking - Admin'),
          accRow('paypal-cash',      'PayPal Account'),
        ], 'Total Cash'),

        row('receivables', 'Receivables', [
          accRow('grants-receivable',         'Grants Receivable'),
          accRow('stripe-receivables',        'Stripe Receivables'),
          accRow('paypal-giving-receivables', 'PayPal Giving'),
        ], 'Total Receivables'),

        row('fixed', 'Fixed Assets', [
          accRow('equipment-assets',       'Computers & Accessories'),
          accRow('kenya-hq-prepaid-lease', 'Kenya HQ Prepaid Lease'),
        ], 'Total Fixed Assets'),
      ], 'Total Assets'),

      row('lna', 'Liabilities Net Assets', [
        row('liabilities', 'Liabilities', [
          accRow('gusto-payable',             'Accrued Payroll'),
          accRow('techlit-providers-payable', 'TechLit Kenya Payables'),
          accRow('reimbursable-expenses',     'Reimbursable Expenses'),
          // accRow('other-payables',            'Other Payables'),
        ], 'Total Liabilities'),

        row('net-assets', 'Net Assets', [
          accRow('unrestricted', 'Unrestricted'),
          row('restricted',      'Restricted'),

          row('res', 'Restrictions', [
            accRow('admin-net-assets',   'Admin'),
            accRow('schools-net-assets', 'Schools'),
            accRow('other-net-assets',   'Other'),
          ]),

          // row('schools-breakdown', 'Schools', [
          //   accRow('iex-schools-net-assets', 'IEX Schools'),
          //   accRow('chepyuan-school-net-assets', 'Chepyuan School'),
          //   accRow('logiri-school-net-assets', 'Logiri School'),
          // ]),
          // row('other-breakdown', 'Other', [
          //   // accRow('shipping-net-assets', 'Shipping & Customs'),
          //   accRow('kenya-hq-net-assets', 'Kenya HQ'),
          //   accRow('media-studio-net-assets', 'Media Studio'),
          //   accRow('student-scholarships-net-assets', 'Student Scholarships'),
          // ]),

          {...row('net-assets-change', 'Change in Net Assets', [
            {...row('unrestricted-change', 'Unrestricted'), type: 'change'},
            {...row('restricted-change', 'Restricted'), type: 'change'},
          ], 'Total Change in Net Assets'),
            type: 'change'},
        ], 'Total Net Assets'),
      ], 'Total Liabilities & Net Assets'),
    ],

    cols: [
      col('2023', [], [
        col('2023-12', [
          t('EOY', 'text-sm text-gray-400 center'),
          t('2023', 'justify-end'),
        ]),
      ]),
      col('2024-breakdown', [], [
        ...us2024Periods
        .filter(({id}) => id != '2024')
        .filter(({end}) => end <= us2024.value.settings.latest)
        .map(({id, name}) =>
          col(id, [
            t('As of', 'h-4 text-sm text-gray-400 justify-center'),
            t(name, 'justify-end'),
          ]),
        ),
      ]),
      // col('2024', [], [
      // ], [t('EOY', 'text-base opacity-50 center'), t('2024', 'justify-end')]),
    ],

    rowSums: {
      'restricted': rowSum(
        'admin-net-assets',
        'schools-net-assets',
        'other-net-assets',
      ),

      'net-assets': rowSum('restricted', 'unrestricted'),

      'lna': ({cells}, colId, rowId) => {
        const col = cells[colId]
        col[rowId] = col['liabilities'] + col['net-assets']
      },

      'net-assets-change': ({cells}, colId, rowId) => {
        const curr = cells[colId]

        if (colId == '2023-12' || colId == '2023' || colId == '2024') {
          curr.change = 0
        } else {
          const prevId = colId == '2024-01' ? '2023-12' : prevUs2024Period(colId)
          const prev = cells[prevId]

          curr['unrestricted-change'] = curr['unrestricted'] - prev['unrestricted']
          curr['restricted-change'] = curr['restricted'] - prev['restricted']
          curr[rowId] = curr['net-assets'] - prev['net-assets']
        }
      },
    },

    mapData: ({accountsLookup, cells}, {date, acc, dr, cr}) => {
      const colId = dateToUs2024Period(date)
      const account = accountsLookup[acc]
      const rowId = rowIdForAccountName(acc)
      cells[colId][rowId] += account.nat_dr ? dr - cr : cr - dr
    },
  })

export const Us2024PositionByMonth = () => pug`
  Report(report=us2024PositionByMonth() fmt='usd')
    .text-3xl.font-bold Statement of Position
    .row.flex-wrap.gap-x-4
      = $active('/us2024/position-by-month', 'Monthly')
      = $nav('/us2024/position-by-fund/latest', 'Restricted')
      = $nav('/us2024/cash-balances/latest', 'Cash Position')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/us2024/activity-by-month') (Activity)
`
export default Us2024PositionByMonth
