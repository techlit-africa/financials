import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'
import {Us2024Layout} from '@/layouts/Us2024Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date, titleize} from '@/lib'
import {dateToUs2024Period, getUs2024Period, nextUs2024Period, prevUs2024Period, us2024} from '@/journals/us2024'

const us2024TopChannels = (channels, period) => createReport({
  journalSignal: us2024,

  rows: [
    row('all', 'Revenue Channels', [
      ...channels.map(({name}) => row(name, titleize(name))),
      row('other', 'Other'),
    ], 'All Channels'),
  ],

  rowSorter: (cells, a, b) => cells['closing'][b.id] - cells['closing'][a.id],

  rowFilter: (cells, row) => row.id == 'all' || cells['closing'][row.id] > 0,

  cols: [
    col('all', [], [
      {type: 'change', ...col('current', [t(`${date(period.start)} - ${date(period.end)}`, 'center')], [
        {type: 'change', ...col('monetary-donations', [t('Monetary', 'font-bold center')])},
        {type: 'change', ...col('equipment-donations', [t('Equipment', 'font-bold center')])},
        {type: 'change', ...col('other-revenue', [t('Other', 'font-bold center')])},
      ], [t('Total', 'font-bold center')])},

      col('closing', [t(`2024 Total`, 'center')], [
        col('closing-monetary-donations', [t('Monetary', 'font-bold center')]),
        col('closing-equipment-donations', [t('Equipment', 'font-bold center')]),
        col('closing-other-revenue', [t('Other', 'font-bold center')]),
      ], [t('Total', 'font-bold center')]),
    ])
  ],

  mapData: ({accountsLookup, cells}, {date, acc, channel, dr, cr}) => {
    if (date > period.end) return
    if (acc != 'monetary-donations' && acc != 'equipment-donations' && acc != 'other-revenue') return

    cells[`closing-${acc}`][channel || 'other'] += cr - dr
    if (date >= period.start) cells[acc][channel || 'other'] += cr - dr
  },
})

export const Us2024TopChannels = ({periodId}) => pug`
  - const period = getUs2024Period(periodId)
  - const prevPeriodId = prevUs2024Period(period.id)
  - const nextPeriodId = nextUs2024Period(period.id)

  Report(report=us2024TopChannels(us2024.value.channels, period) fmt='usd')
    .text-3xl.pb-4.font-bold Top Channels
    .row.gap-x-4.flex-wrap
      = $nav('/us2024/cash-balances/'+periodId, 'Cash Balances')
      = $active('/us2024/top-channels/'+periodId, 'Top Channels')
      = $nav('/us2024/top-donors/'+periodId, 'Top Donors')
      = $nav('/us2024/salaries/'+periodId, 'Salaries')
      = $nav('/us2024/reimbursements/'+periodId, 'Reimbursements')
      = $nav('/us2024/kenya-payables/'+periodId, 'Providers')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2024/top-channels/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2024/top-channels/'+nextPeriodId, 'Next')
`
export default Us2024TopChannels
