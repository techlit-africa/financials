import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'
import {Us2024Layout} from '@/layouts/Us2024Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date, titleize} from '@/lib'
import {dateToUs2024Period, getUs2024Period, nextUs2024Period, prevUs2024Period, us2024} from '@/journals/us2024'

const us2024Reimbursements = (parties, period) => {
  const employees = []
  parties.forEach(({name, kind}) => {
    if (kind != 'employee') return
    employees.push(name)
  })
  
  return createReport({
    journalSignal: us2024,

    rows: [
      row('employees', 'Employees', [
        ...employees.map((name) => row(name, name)),
      ], 'Total'),
    ],

    cols: [
      col('summary', [], [
        col('opening', [t(`Before ${date(period.start)}`, 'h-32 font-bold justify-center'),]),
        {...col('received', [t('Received', 'h-32 font-bold justify-center'),]), type: 'change'},
        {...col('expensed', [t('Expensed', 'h-32 font-bold justify-center'),]), type: 'change'},
      ]),
      col('closing', [], [], [t(`As of ${date(period.end)}`, 'h-32 font-bold justify-center',)]),
    ],

    mapData: ({accountsLookup, cells}, {date, acc, party, dr, cr}) => {
      if (date > period.end) return
      if (!party || acc != 'reimbursable-expenses') return

      cells['closing'][party] += cr - dr

      if (date < period.start) {
        cells['opening'][party] += cr - dr
      } else {
        if (dr) cells['received'][party] += dr
        if (cr) cells['expensed'][party] -= cr
      }
    },
  })
}

export const Us2024Reimbursements = ({periodId}) => pug`
  - const period = getUs2024Period(periodId)
  - const prevPeriodId = prevUs2024Period(period.id)
  - const nextPeriodId = nextUs2024Period(period.id)

  Report(report=us2024Reimbursements(us2024.value.parties, period) fmt='usd')
    .text-3xl.pb-4.font-bold Reimbursements
    .row.gap-x-4.flex-wrap
      = $nav('/us2024/cash-balances/'+periodId, 'Cash Balances')
      = $nav('/us2024/top-channels/'+periodId, 'Top Channels')
      = $nav('/us2024/top-donors/'+periodId, 'Top Donors')
      = $nav('/us2024/salaries/'+periodId, 'Salaries')
      = $active('/us2024/reimbursements/'+periodId, 'Reimbursements')
      = $nav('/us2024/kenya-payables/'+periodId, 'Kenya Payables')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2024/reimbursements/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2024/reimbursements/'+nextPeriodId, 'Next')
`
export default Us2024Reimbursements
