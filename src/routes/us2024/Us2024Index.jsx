import {$btn, $nav} from '@/comps/Macros'
import {Us2024Layout} from '@/layouts/Us2024Layout'

export const Us2024Index = () => pug`
  .row.max-w-screen-md.flex-wrap.gap-12.mt-12
    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Statements of Position
      = $nav('/us2024/position-by-month', 'Monthly Position')
      = $nav('/us2024/position-by-fund/latest', 'Restricted Position')
      = $nav('/us2024/cash-balances/latest', 'Cash Position')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Statements of Activity
      = $nav('/us2024/activity-by-month', 'Monthly Activity')
      = $nav('/us2024/activity-by-fund/latest', 'Restricted Activity')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Journals & Ledgers
      = $nav('/us2024/journal', 'General Journal')
      = $nav('/us2024/accounts', 'Account Ledgers')
      
    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Other Reports
      = $nav('/us2024/top-channels/latest', 'Top Channels')
      = $nav('/us2024/top-donors/latest', 'Top Donors')
      = $nav('/us2024/salaries/latest', 'Salaries')
      = $nav('/us2024/reimbursements/latest', 'Reimbursements')
      = $nav('/us2024/kenya-payables/latest', 'Kenya Payables')
`
export default Us2024Index
