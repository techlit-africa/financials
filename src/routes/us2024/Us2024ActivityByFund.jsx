import {Report, row, accRow, col, t, createReport, colSum} from '@/comps/Report'
import {Us2024Layout} from '@/layouts/Us2024Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date} from '@/lib'

import {dateToUs2024Period, getUs2024Period, nextUs2024Period, prevUs2024Period, us2024} from '@/journals/us2024'

const rowIdForAccountName = (name) => ({
  'techlit-equipment-storage-expenses': 'techlit-other-expenses',

  'fundraising-marketing-expenses':   'fundraising-other-expenses',
  'fundraising-advertising-expenses': 'fundraising-other-expenses',
})[name] || name

const colIdForFundName = (name) => ({
  'general': 'unrestricted',
})[name] || name

const us2024ActivityByFund = (period) => createReport({
  journalSignal: us2024,

  rows: [
    row('activity', 'Restricted Activity', [
      row('rev', 'Revenue', [
        accRow('monetary-donations',  'Monetary Donations'),
        accRow('equipment-donations', 'Donated Computers'),
        accRow('exchange-gains',      'Exchange Gains (Losses)'),
        accRow('other-revenue',       'Other Revenue'),
      ], 'Total Revenue'),

      row('exp', 'Expenses', [
        row('techlit', 'TechLit Program', [
          accRow('techlit-salaries',                        'Salaries'),
          accRow('techlit-travel-expenses',                 'Travel'),
          accRow('techlit-cloud-services-expenses',         'Cloud Services'),
          accRow('techlit-equipment-shipping-expenses',     'Shipping'),
          accRow('techlit-equipment-depreciation-expenses', 'Depreciation'),
          accRow('techlit-provider-fees-expenses',          'TechLit Kenya Fees'),
          accRow('techlit-provider-setup-expenses',         'TechLit Kenya Setup'),
          accRow('techlit-other-expenses',                  'Other'),
        ], 'TechLit Program Total'),

        row('other', 'Other Programs', [
          accRow('kenya-hq-rent-expenses',       'Kenya HQ Rent'),
          accRow('media-studio-setup-expenses',  'Media Studio'),
          accRow('student-scholarship-expenses', 'Student Support'),
          accRow('other-program-expenses',       'Other'),
        ], 'Other Programs Total'),

        row('mgmt', 'Management', [
          accRow('mgmt-salaries',                'Salaries'),
          accRow('mgmt-payroll-taxes',           'Payroll Taxes'),
          accRow('mgmt-money-fees-expenses',     'Money Fees'),
          accRow('mgmt-admin-services-expenses', 'Admin Services'),
          accRow('mgmt-cloud-services-expenses', 'Cloud Services'),
          accRow('mgmt-other-expenses',          'Other'),
        ], 'Management Total'),

        row('fundraising', 'Fundraising', [
          accRow('fundraising-salaries',        'Salaries'),
          accRow('fundraising-travel-expenses', 'Travel'),
          accRow('fundraising-events-expenses', 'Events'),
          accRow('fundraising-other-expenses',  'Other'),
        ], 'Fundraising Total'),
      ], 'Total Expenses'),
    ], 'Surplus (Deficit)'),
  ],

  // rowFilter: (cells, row) => cells['all'][row.id] > 0,

  cols: [
    col('all', [t('Summary', 'center')], [
      col('unrestricted', [t('Unrestricted', 'items-center text-base')]),
      col('restricted',   [t('Restricted',   'items-center text-base')]),
    ], [t('Total', 'center')]),

    col('restricted', [t('Restrictions', 'center')], [
      col('admin', [t('Admin', 'center font-bold')]),
      col('schools', [t('Schools', 'center font-bold')]),
      col('other', [t('Other', 'center font-bold')]),

      col('schools-breakdown', [t('School Support', 'center')], [
        col('iex',      [t('IEX',      'center')]),
        col('logiri',   [t('Logiri',   'center')]),
        col('chepyuan', [t('Chepyuan', 'center')]),
      ]),

      col('other-breakdown', [t('Other Restrictions', 'center')], [
        col('shipping',             [t('Shipping',     'center')]),
        col('kenya-hq',             [t('Kenya HQ',     'center')]),
        col('media-studio',         [t('Media Studio', 'center')]),
        col('student-scholarships', [t('Students',     'center')]),
      ]),
    ]),
  ],

  mapData: ({cells, accountsLookup}, {date, fund, acc, dr, cr, note}) => {
    if (date < period.start || date > period.end) return
    const rowId = rowIdForAccountName(acc)
    const colId = colIdForFundName(fund)
    const {nat_dr} = accountsLookup[acc]
    cells[colId][rowId] += nat_dr ? dr - cr : cr - dr
  },

  rowSums: {
    'activity': ({cells}, colId, rowId) => {
      const col = cells[colId]
      col[rowId] = col['rev'] - col['exp']
    },
  },

  colSums: {
    'schools': colSum('iex', 'logiri', 'chepyuan'),
    'other': colSum('shipping', 'kenya-hq', 'media-studio', 'student-scholarships'),
    'restricted': colSum('admin', 'schools', 'other'),
    'all': colSum('unrestricted', 'restricted'),
  },
})

export const Us2024ActivityByFund = ({periodId}) => pug`
  - const period = getUs2024Period(periodId)
  - const nextPeriodId = nextUs2024Period(period.id)
  - const prevPeriodId = prevUs2024Period(period.id)

  Report(report=us2024ActivityByFund(period) fmt='usd')
    .h-full.col
      .text-3xl.font-bold Statement of Activity
      .row.gap-4
        = $nav('/us2024/activity-by-month', 'Monthly')
        = $active('/us2024/activity-by-fund/latest', 'Restricted')
        a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/us2024/position-by-fund/latest') (Position)
      .row.gap-4.justify-between.pt-3
        = (prevPeriodId ? $nav : $disabled)('/us2024/activity-by-fund/'+prevPeriodId, 'Prev')
        .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
        = (nextPeriodId ? $nav : $disabled)('/us2024/activity-by-fund/'+nextPeriodId, 'Next')
`
export default Us2024ActivityByFund
