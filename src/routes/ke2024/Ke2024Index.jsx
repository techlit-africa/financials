import {$btn, $nav} from '@/comps/Macros'
import {Ke2024Layout} from '@/layouts/Ke2024Layout'

export const Ke2024Index = () => pug`
  .row.max-w-screen-md.flex-wrap.gap-12.mt-12
    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Statements of Position
      = $nav('/ke2024/position-by-month', 'Monthly Position')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Statements of Activity
      = $nav('/ke2024/activity-by-month', 'Monthly Activity')
      = $nav('/ke2024/activity-by-school/latest', 'School Activity')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Journals & Ledgers
      = $nav('/ke2024/journal', 'General Journal')
      = $nav('/ke2024/accounts', 'Account Ledgers')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Other Reports
      = $nav('/ke2024/stipends/latest', 'Stipends')
      = $nav('/ke2024/petty-cash/latest', 'Cash / Expenses')
      = $nav('/ke2024/vendor-payables/latest', 'Vendors')
      = $nav('/ke2024/rent-payables/latest', 'Rent')
      = $nav('/ke2024/school-receivables/latest', 'Schools')
`
export default Ke2024Index
