import {Report, row, accRow, col, t, createReport, colSum, rowSum} from '@/comps/Report'
import {Ke2024Layout} from '@/layouts/Ke2024Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {titleize, date} from '@/lib'

import {
  ke2024,
  ke2024Periods,
  getKe2024Period,
  nextKe2024Period,
  prevKe2024Period,
  dateToKe2024Period,
} from '@/journals/ke2024'

const ke2024RentPayables = (allSchools, period) => {
  const schools = []

  allSchools.forEach((school) => {
    if (school.tier == '0') return
    schools.push(school.name)
  })

  return createReport({
    journalSignal: ke2024,

    rows: [
      col(
        'summary',
        'Summary',
        [...schools.map((name) => col(name, titleize(name)))],
        'Total',
      ),
    ],

    rowFilter: (cells, row) =>
      period == '2024' ||
        row.id == 'summary' ||
        cells['opening'][row.id] != 0 ||
        cells['paid'][row.id] != 0 ||
        cells['expensed'][row.id] != 0 ||
        cells['closing'][row.id] != 0,

    cols: [
      col(
        'summary',
        [],
        [
          col('opening', [t('Opening', 'h-28 font-bold center')]),
          {
            ...col('paid', [t('Paid', 'h-28 font-bold center')]),
            type: 'change',
          },
          {
            ...col('expensed', [t('Expensed', 'h-28 font-bold center')]),
            type: 'change',
          },
        ],
      ),

      col('closing', [], [], [t('Closing', 'font-bold')]),
    ],

    mapData: ({cells, accountsLookup}, {date, school, acc, dr, cr}) => {
      if (date > period.end) return
      if (!school || acc != 'rent-expense') return

      cells['closing'][school] += dr - cr

      if (date < period.start) {
        cells['opening'][school] += dr - cr
      } else {
        if (dr) cells['paid'][school] += dr
        if (cr) cells['expensed'][school] -= cr
      }
    },

    rowSums: {
      summary: rowSum(...schools),
    },
  })
}

export const Ke2024RentPayables = ({periodId}) => pug`
  - const period = getKe2024Period(periodId)
  - const nextPeriodId = nextKe2024Period(period.id)
  - const prevPeriodId = prevKe2024Period(period.id)

  Report(report=ke2024RentPayables(ke2024.value.schools, period))
    .text-3xl.font-bold Rent Payables
    .row.gap-x-4.flex-wrap
      = $nav('/ke2024/stipends/'+periodId, 'Stipends')
      = $nav('/ke2024/petty-cash/'+periodId, 'Cash / Expenses')
      = $nav('/ke2024/vendor-payables/'+periodId, 'Vendors')
      = $active('/ke2024/rent-payables/'+periodId, 'Rent')
      = $nav('/ke2024/school-receivables/'+periodId, 'Schools')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2024/rent-payables/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2024/rent-payables/'+nextPeriodId, 'Next')
`
export default Ke2024RentPayables
