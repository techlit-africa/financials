import {Ke2024Layout} from '@/layouts/Ke2024Layout'
import {ke2024} from '@/journals/ke2024'

const $nav = ({name, description}) => pug`
  a.row.items-center.gap-1.text-3xl.font-bold(key=name href='/ke2024/accounts/'+name) [ #[.text-lg.text-blue-700= description || name] ]
`

export const Ke2024Accounts = () => pug`
  - const {accounts} = ke2024.value

  .flex-1.col.items-start.gap-2.p-8.overflow-y-scroll
    h2.inline.px-1.text-2xl.font-semibold.border-b-4.border-yellow-300 Accounts
    for account in accounts || []
      if account.name
        = $nav(account)
`
export default Ke2024Accounts
