import {Ke2024Layout} from '@/layouts/Ke2024Layout'
import {ke2024Accounts} from '@/journals/ke2024'
import {Account, Line, $tag} from '@/comps/Account'
import {Failed, Loading} from '@/comps/Suspense'

export const Ke2024Account = ({name}) => pug`
  - const account = ke2024Accounts.value[name]

  .flex-1.col.gap-2.overflow-y-scroll
    unless account
      Loading

    else
      Account(account=account)
        for line in account.lines
          Line(key=line.id line=line)
            if line.fund != 'general'
              = $tag('$', line.fund, 'bg-pink-100')
            = $tag('@', line.people, 'bg-blue-100')
            = $tag('#', line.school, 'bg-orange-100')
`

export default Ke2024Account
