import {Report, row, accRow, col, t, createReport} from '@/comps/Report'
import {Ke2024Layout} from '@/layouts/Ke2024Layout'
import {$active, $nav} from '@/comps/Macros'

import {ke2024, ke2024Periods, dateToKe2024Period, prevKe2024Period} from '@/journals/ke2024'

const rowIdForAccountName = (name) =>
  ({
    'general-net-assets': 'unrestricted',

    'mri-net-assets': 'restricted',
    'headquarters-net-assets': 'restricted',
    'media-studio-net-assets': 'restricted',
    'iex-schools-net-assets': 'restricted',
    'logiri-school-net-assets': 'restricted',
    'chepyuan-school-net-assets': 'restricted',
  })[name] || name

const ke2024PositionByMonth = () =>
  createReport({
    journalSignal: ke2024,

    isCumulative: true,

    rows: [
      row(
        'assets',
        'Assets',
        [
          row(
            'cash',
            'Cash',
            [
              accRow('kcb-checking', 'KCB Checking'),
              accRow('employee-advance', 'Employee Advance'),
            ],
            'Total Cash',
          ),

          row(
            'receivables',
            'Receivables',
            [
              accRow('school-fees-receivable', 'School Fees'),
              // accRow('scholarships-receivable', 'Scholarships'),
              // accRow('other-receivables', 'Other'),
            ],
            'Total Receivables',
          ),

          row(
            'fixed',
            'Fixed Assets',
            [
              // accRow('prepaid-rent', 'Prepaid Rent'),
              accRow('motorbike-assets', 'Motorbikes'),
              accRow('furniture-assets', 'Furniture'),
              accRow('equipment-assets', 'Equipment'),
              accRow('media-studio-assets', 'Media Studio'),
            ],
            'Total Fixed Assets',
          ),
        ],
        'Total Assets',
      ),

      row(
        'lna',
        'Liabilities & Net Assets',
        [
          row(
            'liabilities',
            'Liabilities',
            [
              row(
                'prepayments',
                'Prepaid Services',
                [
                  accRow('prepaid-school-fees', 'School Fees'),
                  // accRow('prepaid-scholarships', 'Scholarships'),
                ],
                'Total Prepaid Services',
              ),

              row(
                'payables',
                'Payables',
                [
                  accRow('stipends-payable', 'Stipends Payable'),
                  accRow('expenses-reimbursable', 'Expenses Reimbursable'),
                  accRow('vendor-payables', 'Vendors Payable'),
                  // accRow('other-payables', 'Other'),
                ],
                'Total Payables',
              ),
            ],
            'Total Liabilities',
          ),

          row(
            'net-assets',
            'Net Assets',
            [
              accRow('unrestricted', 'Unrestricted'),
              accRow('restricted', 'Restricted'),
            ],
            'Total Net Assets',
          ),

          {
            ...row('net-assets-change', 'Total Change in Net Assets'),
            type: 'change',
          },
        ],
        'Total Liabilities & Net Assets',
      ),
    ],

    cols: [
      col(
        '2023',
        [],
        [
          col('2023-12', [
            t('As of', 'h-4 text-sm text-gray-400 justify-start'),
            t('Dec 31, 2023', 'justify-end'),
          ]),
        ],
      ),

      col(
        '2024',
        [],
        [
          ...ke2024Periods
            .filter(({id}) => id != '2024')
            .filter(({end}) => end <= ke2024.value.settings.latest)
            .map(({id, name}) =>
              col(id, [
                t('As of', 'h-4 text-sm text-gray-400 justify-center'),
                t(name, 'justify-end'),
              ]),
            ),
        ],
      ),

      // col(
      //   'ytd',
      //   [],
      //   [],
      //   [
      //     t('As of', 'h-4 text-sm text-yellow-800 justify-start'),
      //     t('Dec 31, 2024', 'justify-end'),
      //   ],
      // ),
    ],

    mapData: ({cells, accountsLookup}, {date, acc, dr, cr}) => {
      if (date > ke2024.value.settings.latest) return
      const colId = dateToKe2024Period(date)
      const rowId = rowIdForAccountName(acc)
      const {nat_dr} = accountsLookup[acc]
      console.log({colId, rowId, cells})
      cells[colId][rowId] += nat_dr ? dr - cr : cr - dr
    },

    rowSums: {
      'lna': ({cells}, colId, rowId) => {
        const col = cells[colId]
        col[rowId] = col['liabilities'] + col['net-assets']
      },

      'net-assets-change': ({cells}, colId, rowId) => {
        const curr = cells[colId]

        if (colId == '2023-12' || colId == '2023' || colId == '2024') {
          curr.change = 0
        } else {
          const prevId = colId == '2024-01' ? '2023-12' : prevKe2024Period(colId)
          const prev = cells[prevId]

          console.log({colId, rowId, prev})
          curr[rowId] = curr['net-assets'] - prev['net-assets']
        }
      },
    },
  })

export const Ke2024PositionByMonth = () => pug`
  Report(report=ke2024PositionByMonth())
    .text-3xl.font-bold Statement of Position
    .row.gap-4
      = $active('/ke2024/position-by-month', 'Monthly')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/ke2024/activity-by-month') (Activity)
`
export default Ke2024PositionByMonth
