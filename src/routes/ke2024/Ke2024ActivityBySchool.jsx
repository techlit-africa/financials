import {
  Report,
  row,
  accRow,
  col,
  t,
  createReport,
  colSum,
  rowSum,
} from '@/comps/Report'
import {Ke2024Layout} from '@/layouts/Ke2024Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {titleize, date} from '@/lib'

import {
  ke2024,
  ke2024Periods,
  getKe2024Period,
  nextKe2024Period,
  prevKe2024Period,
  dateToKe2024Period,
} from '@/journals/ke2024'

const rowIdForAccountName = (name) => ({
  'cash-donation-revenue': 'other-revenue'
})[name] || name

const ke2024ActivityBySchool = (receivables, period) => {
  const schoolByName = {}
  const schoolNamesByKind = {}

  receivables.forEach((school) => {
    if (!school.segment) return

    if (!schoolNamesByKind[school.segment]) schoolNamesByKind[school.segment] = []
    schoolNamesByKind[school.segment].push(school.name)
    schoolByName[school.name] = school
  })
  const kinds = Object.keys(schoolNamesByKind)

  return createReport({
    journalSignal: ke2024,

    rows: [
      row('net', 'Activity', [
        row('rev', 'Revenue', [
          accRow('school-fees-revenue', 'School Fees'),
          accRow('school-grants-revenue', 'School Grants'),
          accRow('other-revenue', 'Donations & Other'),
        ], 'Total Revenue'),

        row('exp', 'Expenses', [
          row('teaching', 'Teaching', [
            row('emp-retainer-pay', 'Weekly Retainer'),
            row('emp-teaching-pay', 'Daily Commission'),
            row('emp-targets-pay', 'Targets Bonus'),
            row('emp-relocation-pay', 'Relocation Bonus'),
            row('emp-other-pay', 'Other Bonuses'),
          ], 'Total Teaching'),

          row('training', 'Training', [
            row('emp-training-pay', 'Commission'),
            row('training-travel-exp', 'Travel'),
            row('training-lodging-exp', 'Lodging'),
            row('training-food-exp', 'Food'),
            row('training-other-exp', 'Other'),
          ], 'Total Training'),

          row('hiring', 'Hiring', [
            row('interviews-commission-pay', 'Stipends'),
            row('interviews-travel-exp', 'Travel'),
            row('interviews-lodging-exp', 'Lodging'),
            row('interviews-other-exp', 'Other'),
          ], 'Total Hiring'),

          row('support', 'Support', [
            row('support-shipping-exp', 'Shipping'),
            row('support-supplies-exp', 'Supplies'),
            row('support-installs-exp', 'Installs'),
            row('support-travel-exp', 'Travel'),
            row('support-other-exp', 'Other'),
          ], 'Total Support'),

          row('money-fees-exp', 'Transaction Fees'),
          row('depreciation-exp', 'Depreciation'),
          row('other-exp', 'Other'),
        ], 'Total Expenses'),
      ], 'Net Surplus (Deficit)'),

      // row('summary', 'Summary', [
      //   {...row('month-margin', 'Monthly', [
      //     row('month-rev', 'Revenue'),
      //     row('month-exp', 'Expenses'),
      //     row('month-net', 'Surplus (Deficit)'),
      //   ], 'Margin'), type: 'percent'},
      // ]),
    ],

    cols: [
      col('all', [t('Summary', 'center')], [
        col('shared', [t('Shared', 'font-bold center')]),
        col('schools', [t('Schools', 'font-bold center')]),
      ], [t('Total', 'center')]),

      col('schools-breakdown', [t('Schools', 'center')], [
        ...kinds.map((kind) => col(kind, [t(titleize(kind), 'font-bold center')])),

        ...kinds.map((kind) => col(`${kind}-breakdown`, [t(titleize(kind), 'center')], [
          ...schoolNamesByKind[kind].map((name) => col(name, [t(titleize(name), 'center')])),
        ])),
      ]),
    ],

    colFilter: (cells, col) => {
      if (col.id == 'all' || col.id == 'shared') return true
      if (col.id.endsWith('-breakdown')) {
        const summaryId = col.id.replace('-breakdown', '')
        return cells[summaryId]['rev'] != 0 || cells[summaryId]['exp'] != 0
      }
      return cells[col.id]['rev'] != 0 || cells[col.id]['exp'] != 0
    },

    mapData: ({cells, accountsLookup}, {date, receivable, acc, dr, cr}) => {
      if (date < period.start || date > period.end) return
      const colId = schoolByName[receivable]?.name || 'shared'
      const rowId = rowIdForAccountName(acc)
      const {nat_dr} = accountsLookup[acc]
      cells[colId][rowId] += nat_dr ? dr - cr : cr - dr
    },

    colSums: {
      ...kinds.reduce((o, kind) => {
        o[kind] = colSum(...schoolNamesByKind[kind])
        return o
      }, {}),

      schools: colSum(...kinds),
      all: colSum('shared', 'schools'),
    },

    rowSums: {
      net: ({cells}, colId, rowId) => {
        const col = cells[colId]
        col[rowId] = col['rev'] - col['exp']
      },

      // 'month-rev': rowSum('rev'),
      // 'month-exp': rowSum('exp'),
      // 'month-net': rowSum('net'),
      // 'month-margin': ({cells}, colId, rowId) => {
      //   const col = cells[colId]
      //   col[rowId] = col['net'] / col['rev']
      // },
    },
  })
}

export const Ke2024ActivityBySchool = ({periodId}) => pug`
  - const period = getKe2024Period(periodId)
  - const nextPeriodId = nextKe2024Period(period.id)
  - const prevPeriodId = prevKe2024Period(period.id)

  Report(report=ke2024ActivityBySchool(ke2024.value.receivables, period))
    .text-3xl.font-bold Statement of Activity
    .row.gap-4
      = $nav('/ke2024/activity-by-month', 'Monthly')
      = $active('/ke2024/activity-by-school/latest', 'Schools')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/ke2024/position-by-month') (Position)
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2024/activity-by-school/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2024/activity-by-school/'+nextPeriodId, 'Next')
`
export default Ke2024ActivityBySchool
