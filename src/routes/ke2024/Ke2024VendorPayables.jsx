import {Report, createReport, col, row, t, rowSum} from '@/comps/Report'
import {
  ke2024,
  getKe2024Period,
  nextKe2024Period,
  prevKe2024Period,
} from '@/journals/ke2024'
import {Ke2024Layout} from '@/layouts/Ke2024Layout'
import {$nav, $disabled, $active} from '@/comps/Macros'
import {titleize, date} from '@/lib'

const ke2024VendorPayables = (people, period) => {
  const vendors = []
  people.forEach((p) => {
    if (p.kind == 'vendors') vendors.push(p)
  })

  return createReport({
    journalSignal: ke2024,

    rowFilter: (cells, row) =>
      period == '2024' ||
        cells['opening'][row.id] != 0 ||
        cells['paid'][row.id] != 0 ||
        cells['billed'][row.id] != 0 ||
        cells['closing'][row.id] != 0,

    rows: [
      row(
        'vendors',
        'Vendors',
        [...vendors.map(({name}) => row(name, name))],
        'Total',
      ),
    ],

    cols: [
      col(
        'summary',
        [],
        [
          col('opening', [t('Opening', 'h-28 font-bold center')]),
          col('paid', [t('Paid', 'h-28 font-bold center')]),
          col('billed', [t('Billed', 'h-28 font-bold center')]),
        ],
      ),

      col('closing', [], [], [t('Closing', 'font-bold')]),
    ],

    mapData: ({lines, cells, accountsLookup}, {date, acc, people, dr, cr}) => {
      if (date > period.end) return
      if (!people || acc != 'vendor-payables') return

      cells['closing'][people] += cr - dr

      if (date < period.start) {
        cells['opening'][people] += cr - dr
      } else {
        if (dr) cells['paid'][people] -= dr
        if (cr) cells['billed'][people] += cr
      }
    },
  })
}

export const Ke2024VendorPayables = ({periodId}) => pug`
  - const period = getKe2024Period(periodId)
  - const nextPeriodId = nextKe2024Period(period.id)
  - const prevPeriodId = prevKe2024Period(period.id)

  Report(report=ke2024VendorPayables(ke2024.value.people, period))
    .text-3xl.font-bold Vendor Payables
    .row.gap-x-4.flex-wrap
      = $nav('/ke2024/stipends/'+periodId, 'Stipends')
      = $nav('/ke2024/petty-cash/'+periodId, 'Cash / Expenses')
      = $active('/ke2024/vendor-payables/'+periodId, 'Vendors')
      = $nav('/ke2024/rent-payables/'+periodId, 'Rent')
      = $nav('/ke2024/school-receivables/'+periodId, 'Schools')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2024/vendor-payables/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2024/vendor-payables/'+nextPeriodId, 'Next')
`
export default Ke2024VendorPayables
