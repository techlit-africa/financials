import {Report, createReport, col, row, t, rowSum} from '@/comps/Report'
import {
  ke2024,
  getKe2024Period,
  nextKe2024Period,
  prevKe2024Period,
} from '@/journals/ke2024'
import {Ke2024Layout} from '@/layouts/Ke2024Layout'
import {$nav, $disabled, $active} from '@/comps/Macros'
import {titleize, date} from '@/lib'

const ke2024Stipends = (people, period) => {
  const educators = []
  const other = []
  people.forEach((p) => (p.kind == 'educators' ? educators : other).push(p))

  return createReport({
    journalSignal: ke2024,

    rows: [
      row('summary', 'Summary', [
          row('educators', 'Educators'),
          row('other', 'Other'),
          row('unknown', 'Unknown'),
        ], 'Total'),

      row('educators', 'Educators',
        [...educators.map(({name}) => row(name, name))],
        'Total'),

      row('other', 'Other',
        [...other.map(({name}) => row(name, name))],
        'Total'),
    ],

    rowFilter: (cells, row) =>
      cells['opening'][row.id] != 0 ||
      cells['paid'][row.id] != 0 ||
      cells['closing'][row.id] != 0,

    cols: [
      col('summary', [], [
          col('opening', [t('Opening', 'h-28 font-bold center')]),
          {...col('paid', [t('Paid', 'h-28 font-bold center')]), type: 'change'},
          {...col('earned', [t('Earned', 'h-28 font-bold center')]), type: 'change'},
        ]),
      col('closing', [], [], [t('Closing', 'font-bold')]),
    ],

    mapData: ({lines, cells, accountsLookup}, {date, acc, people, dr, cr}) => {
      if (date > period.end) return
      if (acc != 'stipends-payable') return
      if (!people) people = 'unknown'

      cells['closing'][people] += cr - dr

      if (date < period.start) {
        cells['opening'][people] += cr - dr
      } else {
        if (dr) cells['paid'][people] -= dr
        if (cr) cells['earned'][people] += cr
      }
    },

    rowSums: {summary: rowSum('other', 'educators', 'unknown')},
  })
}

export const Ke2024Stipends = ({periodId}) => pug`
  - const period = getKe2024Period(periodId)
  - const nextPeriodId = nextKe2024Period(period.id)
  - const prevPeriodId = prevKe2024Period(period.id)

  Report(report=ke2024Stipends(ke2024.value.people, period))
    .text-3xl.font-bold Stipends
    .row.gap-x-4.flex-wrap
      = $active('/ke2024/stipends/'+periodId, 'Stipends')
      = $nav('/ke2024/petty-cash/'+periodId, 'Cash / Expenses')
      = $nav('/ke2024/vendor-payables/'+periodId, 'Vendors')
      = $nav('/ke2024/rent-payables/'+periodId, 'Rent')
      = $nav('/ke2024/school-receivables/'+periodId, 'Schools')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/ke2024/stipends/'+prevPeriodId, 'Prev')
      .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/ke2024/stipends/'+nextPeriodId, 'Next')
`
export default Ke2024Stipends
