import {
  Report,
  row,
  accRow,
  col,
  t,
  createReport,
  rowSum,
  colSum,
} from '@/comps/Report'
import {Ke2024Layout} from '@/layouts/Ke2024Layout'
import {$active, $nav} from '@/comps/Macros'
import {date} from '@/lib'

import {ke2024, ke2024Periods, dateToKe2024Period} from '@/journals/ke2024'

const rowIdForAccountName = (name) => ({
  'cash-donation-revenue': 'other-revenue'
})[name] || name

const ke2024ActivityByMonth = () => {
  const months = ke2024Periods.filter(({id}) => id != '2024')
  // ({end}) => end <= ke2024.value.settings.latest,

  return createReport({
    journalSignal: ke2024,

    rows: [
      row('net', 'Activity', [
        row('rev', 'Revenue', [
          accRow('school-fees-revenue', 'School Fees'),
          accRow('school-grants-revenue', 'School Grants'),
          accRow('other-revenue', 'Donations & Other'),
        ], 'Total Revenue'),

        row('exp', 'Expenses', [
          row('teaching', 'Teaching', [
            row('emp-retainer-pay', 'Weekly Retainer'),
            row('emp-teaching-pay', 'Daily Commission'),
            row('emp-targets-pay', 'Targets Bonus'),
            row('emp-relocation-pay', 'Relocation Bonus'),
            row('emp-other-pay', 'Other Bonuses'),
          ], 'Total Teaching'),

          row('training', 'Training', [
            row('emp-training-pay', 'Commission'),
            row('training-travel-exp', 'Travel'),
            row('training-lodging-exp', 'Lodging'),
            row('training-food-exp', 'Food'),
            row('training-other-exp', 'Other'),
          ], 'Total Training'),

          row('hiring', 'Hiring', [
            row('interviews-commission-pay', 'Stipends'),
            row('interviews-travel-exp', 'Travel'),
            row('interviews-lodging-exp', 'Lodging'),
            row('interviews-other-exp', 'Other'),
          ], 'Total Hiring'),

          row('support', 'Support', [
            row('support-retainer-pay', 'Salaries'),
            row('support-shipping-exp', 'Shipping'),
            row('support-supplies-exp', 'Supplies'),
            row('support-installs-exp', 'Installs'),
            row('support-travel-exp', 'Travel'),
            row('support-other-exp', 'Other'),
          ], 'Total Support'),

          row('money-fees-exp', 'Transaction Fees'),
          row('depreciation-exp', 'Depreciation'),
          row('other-exp', 'Other'),
        ], 'Total Expenses'),
      ], 'Net Surplus (Deficit)'),

      // row('summary', 'Summary', [
      //   {...row('month-margin', 'Monthly', [
      //     row('month-rev', 'Revenue'),
      //     row('month-exp', 'Expenses'),
      //     row('month-net', 'Surplus (Deficit)'),
      //   ], 'Margin'), type: 'percent'},
      // ]),
    ],

    cols: [
      col('prev', [], [
        col('2023', [
          t('Dec 31', 'text-sm opacity-50'),
          t('2023', 'text-lg font-bold'),
        ]),
      ]),

      col('curr', [], [
        col('2024', [
          t('Dec 31', 'text-sm opacity-50'),
          t('2024', 'text-lg font-bold'),
        ]),
      ]),

      col('months', [], [
        ...ke2024Periods
          .filter(({id}) => id != '2024')
          // .filter(({end}) => end <= ke2024.value.settings.latest)
          .map(({id, start, end}) =>
            col(id, [
              t(
                `${date(start)} -`,
                'h-4 text-sm text-gray-400 justify-center',
              ),
              t(date(end), 'justify-end'),
            ]),
          ),
      ]),
    ],

    initData: ({cells}) => {
      cells['2023']['school-fees-revenue'] = 3892500
      cells['2023']['school-grants-revenue'] = 0
      cells['2023']['other-revenue'] = 882

      cells['2023']['emp-retainer-pay'] = 0
      cells['2023']['emp-teaching-pay'] = 2043860
      cells['2023']['emp-targets-pay'] = 0
      cells['2023']['emp-relocation-pay'] = 0
      cells['2023']['emp-other-pay'] = 212700 /* rent */ + 781178 /* transport */

      cells['2023']['emp-training-pay'] = 0
      cells['2023']['training-travel-exp'] = 0
      cells['2023']['training-logding-exp'] = 0
      cells['2023']['training-food-exp'] = 77335
      cells['2023']['training-other-exp'] = 0

      cells['2023']['interviews-commission-pay'] = 0
      cells['2023']['interviews-travel-exp'] = 0
      cells['2023']['interviews-lodging-exp'] = 0
      cells['2023']['interviews-other-exp'] = 0

      cells['2023']['support-shipping-exp'] = 0
      cells['2023']['support-supplies-exp'] = 257420
      cells['2023']['support-installs-exp'] = 281160
      cells['2023']['support-travel-exp'] = 0
      cells['2023']['support-other-exp'] = 0

      cells['2023']['money-fees-exp'] = 64017
      cells['2023']['depreciation-exp'] = 84032
      cells['2023']['other-exp'] = 163883 /* other */ + 63080 /* electricity */

      //   cells['2023']['month-rev'] = 0
      //   cells['2023']['month-exp'] = 0
      //   cells['2023']['month-net'] = 0
      //   cells['2023']['month-margin'] = 0
    },

    mapData: ({cells, accountsLookup}, {date, acc, people, dr, cr}) => {
      if (date < '2024-01-01') return

      const rowId = rowIdForAccountName(acc)
      const colId = dateToKe2024Period(date)
      const {nat_dr} = accountsLookup[acc]
      cells[colId][rowId] += nat_dr ? dr - cr : cr - dr
      cells['2024'][rowId] += nat_dr ? dr - cr : cr - dr
    },

    rowSums: {
      net: ({cells}, colId, rowId) => {
        const col = cells[colId]
        col[rowId] = col['rev'] - col['exp']
      },

      // 'month-rev': rowSum('rev'),
      // 'month-exp': rowSum('exp'),
      // 'month-net': rowSum('net'),
      // 'month-margin': ({cells}, colId, rowId) => {
      //   const col = cells[colId]
      //   col[rowId] = col['net'] / col['rev']
      // },
    },
  })
}

export const Ke2024ActivityByMonth = () => pug`
  Report(report=ke2024ActivityByMonth())
    .text-3xl.font-bold Statement of Activity
    .row.gap-4
      = $active('/ke2024/activity-by-month', 'Monthly')
      = $nav('/ke2024/activity-by-school/latest', 'Schools')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/ke2024/position-by-month') (Position)
`
export default Ke2024ActivityByMonth
