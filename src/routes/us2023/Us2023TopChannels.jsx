import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'
import {Us2023Layout} from '@/layouts/Us2023Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date, titleize} from '@/lib'
import {dateToUs2023Period, getUs2023Period, nextUs2023Period, prevUs2023Period, us2023} from '@/journals/us2023'

const us2023TopChannels = (channels, period) => createReport({
  journalSignal: us2023,

  rows: [
    row('all', 'Revenue Channels', [
      ...channels.map(({name}) => row(name, titleize(name))),
      row('other', 'Other'),
    ], 'All Channels'),
  ],

  rowSorter: (cells, a, b) => cells['closing'][b.id] - cells['closing'][a.id],

  rowFilter: (cells, row) => row.id == 'all' || cells['closing'][row.id] > 0,

  cols: [
    col('all', [], [
      {type: 'change', ...col('current', [t(`${date(period.start)} - ${date(period.end)}`, 'center')], [
        {type: 'change', ...col('monetary-donations', [t('Monetary', 'font-bold center')])},
        {type: 'change', ...col('equipment-donations', [t('Equipment', 'font-bold center')])},
        {type: 'change', ...col('other-revenue', [t('Other', 'font-bold center')])},
      ], [t('Total', 'font-bold center')])},

      col('closing', [t(`2023 Total`, 'center')], [
        col('closing-monetary-donations', [t('Monetary', 'font-bold center')]),
        col('closing-equipment-donations', [t('Equipment', 'font-bold center')]),
        col('closing-other-revenue', [t('Other', 'font-bold center')]),
      ], [t('Total', 'font-bold center')]),
    ])
  ],

  mapData: ({accountsLookup, cells}, {date, acc, channel, dr, cr}) => {
    if (date > period.end) return
    if (acc != 'monetary-donations' && acc != 'equipment-donations' && acc != 'other-revenue') return

    cells[`closing-${acc}`][channel || 'other'] += cr - dr
    if (date >= period.start) cells[acc][channel || 'other'] += cr - dr
  },
})

export const Us2023TopChannels = ({periodId}) => pug`
  - const period = getUs2023Period(periodId)
  - const prevPeriodId = prevUs2023Period(period.id)
  - const nextPeriodId = nextUs2023Period(period.id)

  Report(report=us2023TopChannels(us2023.value.channels, period) fmt='usd')
    .text-3xl.pb-4.font-bold Top Channels
    .row.gap-x-4.flex-wrap
      = $nav('/us2023/cash-balances/'+periodId, 'Cash Balances')
      = $active('/us2023/top-channels/'+periodId, 'Top Channels')
      = $nav('/us2023/top-donors/'+periodId, 'Top Donors')
      = $nav('/us2023/salaries/'+periodId, 'Salaries')
      = $nav('/us2023/reimbursements/'+periodId, 'Reimbursements')
      = $nav('/us2023/kenya-payables/'+periodId, 'Providers')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2023/top-channels/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2023/top-channels/'+nextPeriodId, 'Next')
`
export default Us2023TopChannels
