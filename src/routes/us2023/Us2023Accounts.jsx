import {Us2023Layout} from '@/layouts/Us2023Layout'
import {us2023} from '@/journals/us2023'

const $nav = ({name, description}) => pug`
  a.row.items-center.gap-1.text-3xl.font-bold(key=name href='/us2023/accounts/'+name) [ #[.text-lg.text-blue-700= description || name] ]
`

export const Us2023Accounts = () => pug`
  - const {accounts} = us2023.value

  .flex-1.col.items-start.gap-2.p-8.overflow-y-scroll
    h2.inline.px-1.text-2xl.font-semibold.border-b-4.border-yellow-300 Accounts
    for account in accounts || []
      if account.name
        = $nav(account)
`
export default Us2023Accounts
