import {Report, row, accRow, col, t, createReport, rowSum, colSum} from '@/comps/Report'
import {Us2023Layout} from '@/layouts/Us2023Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date} from '@/lib'

import {dateToUs2023Period, getUs2023Period, nextUs2023Period, prevUs2023Period, us2023} from '@/journals/us2023'

const rowIdForAccountName = (name) => ({
  'general-net-assets':              'net-assets',
  'admin-net-assets':                'net-assets',
  'kenya-hq-net-assets':             'net-assets',
  'media-studio-net-assets':         'net-assets',
  'iex-schools-net-assets':          'net-assets',
  'logiri-school-net-assets':        'net-assets',
  'chepyuan-school-net-assets':      'net-assets',
  'student-scholarships-net-assets': 'net-assets',
  // 'shipping-net-assets':             'net-assets',
})[name] || name

const colIdForFundName = (name) => ({
  'general': 'unrestricted',
})[name] || name

const us2023CashBalances = (period) => createReport({
  journalSignal: us2023,

  rows: [
    row('cash', 'Cash On Hand', [
      accRow('checking-general', 'Checking - General'),
      accRow('checking-admin', 'Checking - Admin'),
      accRow('paypal-cash', 'PayPal Account'),
    ], 'Total Cash On Hand'),

    {...row('owed-and-expected', 'Cash Owed & Expected', [
      {...row('payables', 'Payables', [
        {...accRow('gusto-payable', 'Accrued Payroll'), type: 'change'},
        {...accRow('techlit-providers-payable', 'TechLit Kenya Payables'), type: 'change'},
        {...accRow('reimbursable-expenses', 'Reimbursable Expenses'), type: 'change'},
        {...accRow('other-payables', 'Other Payables'), type: 'change'},
      ], 'Total Payables'), type: 'change'},

      {...row('receivables', 'Receivables', [
        {...accRow('stripe-receivables', 'Stripe Receivables'), type: 'change'},
        {...accRow('paypal-giving-receivables', 'PayPal Giving'), type: 'change'},
        {...accRow('grants-receivable', 'Grants Receivable'), type: 'change'},
      ], 'Total Receivables'), type: 'change'},
    ], 'Total Cash Owed & Expected'), type: 'change'},

    row('summary', 'Summary', [
      row('on-hand', 'On Hand'),
      {...row('owed', 'Owed'), type: 'change'},
      {...row('expected', 'Expected'), type: 'change'},
    ], 'Net Cash Available')
  ],

  rowSums: {
    'on-hand': rowSum('cash'),
    'owed': rowSum('payables'),
    'expected': rowSum('receivables'),
    'summary': rowSum('on-hand', 'owed', 'expected'),
  },

  cols: [
    col('all', [t('Summary', 'center')], [
      col('unrestricted', [t('Unrestricted', 'items-center text-base')]),
      col('restricted',   [t('Restricted',   'items-center text-base')]),
    ], [t('Total', 'h-32 center')]),
  
    col('restricted-breakdown', [t('Restrictions', 'center')], [
      col('admin', [t('Admin', 'center font-bold')]),
      col('schools', [t('Schools', 'center font-bold')]),
      col('other', [t('Other', 'center font-bold')]),
  
      col('schools-breakdown', [t('School Support', 'center')], [
        col('iex',      [t('IEX',      'center')]),
        col('logiri',   [t('Logiri',   'center')]),
        col('chepyuan', [t('Chepyuan', 'center')]),
      ]),
  
      col('other-breakdown', [t('Other Restrictions', 'center')], [
        // col('shipping',             [t('Shipping',     'center')]),
        col('kenya-hq',             [t('Kenya HQ',     'center')]),
        col('media-studio',         [t('Media Studio', 'center')]),
        col('student-scholarships', [t('Students',     'center')]),
      ]),
    ]),
  ],

  colSums: {
    'schools': colSum('iex', 'logiri', 'chepyuan'),
    'other': colSum(
      // 'shipping',
      'kenya-hq',
      'media-studio',
      'student-scholarships',
    ),
    'restricted': colSum('admin', 'schools', 'other'),
    'all': colSum('unrestricted', 'restricted'),
  },

  mapData: ({accountsLookup, cells}, {date, fund, acc, dr, cr}) => {
    if (date > period.end) return
    const {nat_dr} = accountsLookup[acc]
    const colId = colIdForFundName(fund)
    const rowId = rowIdForAccountName(acc)
    cells[colId][rowId] += dr - cr
  },
})

export const Us2023CashBalances = ({periodId}) => pug`
  - const period = getUs2023Period(periodId)
  - const prevPeriodId = prevUs2023Period(period.id)
  - const nextPeriodId = nextUs2023Period(period.id)

  Report(report=us2023CashBalances(period) fmt='usd')
    .pb-4.text-3xl.font-bold Cash Balances
    .row.flex-wrap.gap-x-4
      = $nav('/us2023/position-by-month', 'Monthly')
      = $nav('/us2023/position-by-fund/'+periodId, 'Restricted')
      = $active('/us2023/cash-balances/'+periodId, 'Cash Position')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/us2023/activity-by-fund/'+periodId) (Activity)
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2023/cash-balances/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2023/cash-balances/'+nextPeriodId, 'Next')
`
export default Us2023CashBalances
