import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'
import {Us2023Layout} from '@/layouts/Us2023Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date, titleize} from '@/lib'
import {dateToUs2023Period, getUs2023Period, nextUs2023Period, prevUs2023Period, us2023} from '@/journals/us2023'

const us2023KenyaPayables = (allFunds, period) => {
  const funds = []
  allFunds.forEach(({name, kind}) => {
    funds.push(name)
  })
  console.log(funds)
  
  return createReport({
    journalSignal: us2023,

    rows: [
      row('funds', 'Funds', [
        ...funds.map((name) => row(name, name)),
      ], 'Total'),
    ],
    rowFilter: (cells, row) => (
      row.id == 'all'
        || cells['opening'][row.id] > 0
        || cells['services'][row.id] > 0
        || cells['payments'][row.id] > 0
        || cells['exchange'][row.id] > 0
        || cells['closing'][row.id] > 0
    ),

    cols: [
      col('summary', [], [
        col('opening', [t('Opening', 'h-32 font-bold justify-center'),]),
        {...col('exchange', [t('Exchange', 'h-32 font-bold justify-center'),]), type: 'change'},
        {...col('services', [t('Services', 'h-32 font-bold justify-center'),]), type: 'change'},
        {...col('payments', [t('Payments', 'h-32 font-bold justify-center'),]), type: 'change'},
      ]),
      col('closing', [], [], [t('Closing', 'h-32 font-bold justify-center',)]),
    ],

    mapData: ({accountsLookup, cells}, {date, acc, fund, dr, cr}) => {
      if (date > period.end) return
      if (!fund) return

      if (acc == 'techlit-providers-payable') {
        cells['closing'][fund] += cr - dr
  
        if (date < period.start) {
          cells['opening'][fund] += cr - dr
        } else {
          if (cr) cells['services'][fund] += cr
          if (dr) cells['payments'][fund] -= dr
        }
      } if (acc == 'exchange-gains' && date >= period.start) {
        cells['exchange'][fund] += dr - cr
      }
    },
  })
}

export const Us2023KenyaPayables = ({periodId}) => pug`
  - const period = getUs2023Period(periodId)
  - const prevPeriodId = prevUs2023Period(period.id)
  - const nextPeriodId = nextUs2023Period(period.id)

  Report(report=us2023KenyaPayables(us2023.value.funds, period) fmt='usd')
    .text-3xl.pb-4.font-bold Provider Payables
    .row.gap-x-4.flex-wrap
      = $nav('/us2023/cash-balances/'+periodId, 'Cash Balances')
      = $nav('/us2023/top-channels/'+periodId, 'Top Channels')
      = $nav('/us2023/top-donors/'+periodId, 'Top Donors')
      = $nav('/us2023/salaries/'+periodId, 'Salaries')
      = $nav('/us2023/reimbursements/'+periodId, 'Reimbursements')
      = $active('/us2023/kenya-payables/'+periodId, 'Kenya Payables')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2023/kenya-payables/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2023/kenya-payables/'+nextPeriodId, 'Next')
`
export default Us2023KenyaPayables
