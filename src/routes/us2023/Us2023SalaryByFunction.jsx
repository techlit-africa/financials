import {Report, row, accRow, col, colSum, t, createReport, rowSum} from '@/comps/Report'
import {Us2023Layout} from '@/layouts/Us2023Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date, titleize} from '@/lib'
import {us2023Periods, dateToUs2023Period, getUs2023Period, nextUs2023Period, prevUs2023Period, us2023} from '@/journals/us2023'

const us2023SalaryByFunction = (parties, _) => {
  const employees = []
  parties.forEach((employee) => {
    if (employee.kind != 'employee') return
    employees.push(employee)
  })
  
  return createReport({
    journalSignal: us2023,

    rows: [
      row('employee', 'By Employee', [
        ...employees.map(({name, description}) => row(name, description, [
          row(`${name}-prog`, 'Program'),
          row(`${name}-mgmt`, 'Management'),
          row(`${name}-fndr`, 'Fundraising'),
        ], `${description} Salary`)),
      ]),

      row('salary', 'By Function', [
        row('total-prog', 'Program'),
        row('total-mgmt', 'Management'),
        row('total-fndr', 'Fundraising'),
      ], 'Total Salary'),

      row('total', 'Salary & Tax', [
        row('salary', 'Total Salary'),
        row('taxes', 'Employer Taxes'),
      ], 'Total Cost'),

      // row('balance', 'Accrued Salary & Tax', [
      //   row('opening', 'Opening'),
      //   row('expensed', 'Expensed'),
      //   row('paid', 'Paid'),
      // ], 'Closing'),
    ],

    cols: [
      col('breakdown', [], [
        ...us2023Periods
          .slice(0, -1)
          .map(({id, name}) => col(id, [
            t('As of', 'text-sm text-gray-400 items-end justify-center'),
            t(name, 'justify-end'),
          ]))
      ]),
      col('2023', [], [], [t('YTD', 'text-base opacity-50 center'), t('2023', 'justify-end')]),
  ],

    mapData: ({accountsLookup, cells}, {date, acc, party, dr, cr}) => {
      const colId = dateToUs2023Period(date)

      if (acc == 'techlit-salaries') {
        cells[colId][`${party}-prog`] += dr - cr
        cells[colId]['total-prog'] += dr - cr
      }

      if (acc == 'mgmt-salaries') {
        cells[colId][`${party}-mgmt`] += dr - cr
        cells[colId]['total-mgmt'] += dr - cr
      }

      if (acc == 'fundraising-salaries') {
        cells[colId][`${party}-fndr`] += dr - cr
        cells[colId]['total-fndr'] += dr - cr
      }

      if (acc == 'mgmt-payroll-taxes') {
        cells[colId]['taxes'] += dr - cr
      }
    },

    colSums: {
      '2023': colSum(
        ...us2023Periods
          .filter(({end}) => end <= us2023.value.settings.latest)
          .map(({id}) => id)
      ),
    },

    rowSums: {
      'total-prog': rowSum(...employees.map(({name}) => `${name}-prog`)),
      'total-mgmt': rowSum(...employees.map(({name}) => `${name}-mgmt`)),
      'total-fndr': rowSum(...employees.map(({name}) => `${name}-fndr`)),
    },
  })
}

export const Us2023SalaryByFunction = ({periodId}) => pug`
  - const period = getUs2023Period(periodId)
  - const prevPeriodId = prevUs2023Period(period.id)
  - const nextPeriodId = nextUs2023Period(period.id)

  Report(report=us2023SalaryByFunction(us2023.value.parties, period) fmt='usd')
    .text-3xl.pb-4.font-bold Salaries
    .row.gap-x-4.flex-wrap
      = $nav('/us2023/cash-balances/'+periodId, 'Cash Balances')
      = $nav('/us2023/top-channels/'+periodId, 'Top Channels')
      = $nav('/us2023/top-donors/'+periodId, 'Top Donors')
      = $active('/us2023/salaries/'+periodId, 'Salaries')
      = $nav('/us2023/reimbursements/'+periodId, 'Reimbursements')
      = $nav('/us2023/kenya-payables/'+periodId, 'Kenya Payables')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2023/salaries/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2023/salaries/'+nextPeriodId, 'Next')
`
export default Us2023SalaryByFunction
