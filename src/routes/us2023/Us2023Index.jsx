import {$btn, $nav} from '@/comps/Macros'
import {Us2023Layout} from '@/layouts/Us2023Layout'

export const Us2023Index = () => pug`
  .row.max-w-screen-md.flex-wrap.gap-12.mt-12
    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Statements of Position
      = $nav('/us2023/position-by-month', 'Monthly Position')
      = $nav('/us2023/position-by-fund/latest', 'Restricted Position')
      = $nav('/us2023/cash-balances/latest', 'Cash Position')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Statements of Activity
      = $nav('/us2023/activity-by-month', 'Monthly Activity')
      = $nav('/us2023/activity-by-fund/latest', 'Restricted Activity')

    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Journals & Ledgers
      = $nav('/us2023/journal', 'General Journal')
      = $nav('/us2023/accounts', 'Account Ledgers')
      = $nav('/us2023/trial-balance/latest', 'Trial Balance')
      
    .col.items-start.gap-2
      h2.inline.px-1.text-xl.font-semibold.border-b-4.border-yellow-300 Other Reports
      = $nav('/us2023/top-channels/latest', 'Top Channels')
      = $nav('/us2023/top-donors/latest', 'Top Donors')
      = $nav('/us2023/salaries/latest', 'Salaries')
      = $nav('/us2023/reimbursements/latest', 'Reimbursements')
      = $nav('/us2023/kenya-payables/latest', 'Kenya Payables')
`
export default Us2023Index
