import {Report, row, accRow, col, t, createReport, colSum} from '@/comps/Report'
import {Us2023Layout} from '@/layouts/Us2023Layout'
import {$active, $nav, $disabled} from '@/comps/Macros'

import {dateToUs2023Period, us2023, us2023Periods, getUs2023Period, nextUs2023Period, prevUs2023Period} from '@/journals/us2023'

import {date} from '@/lib'

const us2023ActivityByMonth = (period) =>
  createReport({
    journalSignal: us2023,

    rows: [
      row('trial', 'Trial Balance', [
        row('rev', 'Revenue', [
          accRow('monetary-donations',  'monetary-donations'),
          accRow('equipment-donations', 'equipment-donations'),
          accRow('exchange-gains',      'exchange-gains'),
          accRow('other-revenue',       'other-revenue'),
        ], 'Total Revenue'),

        row('exp', 'Expenses', [
          accRow('techlit-salaries',                        'techlit-salaries'),
          accRow('techlit-travel-expenses',                 'techlit-travel-expenses'),
          accRow('techlit-cloud-services-expenses',         'techlit-cloud-services-expenses'),
          accRow('techlit-equipment-storage-expenses',      'techlit-equipment-storage-expenses'),
          accRow('techlit-equipment-shipping-expenses',     'techlit-equipment-shipping-expenses'),
          accRow('techlit-equipment-depreciation-expenses', 'techlit-equipment-depreciation-expenses'),
          accRow('techlit-provider-fees-expenses',          'techlit-provider-fees-expenses'),
          accRow('techlit-provider-setup-expenses',         'techlit-provider-setup-expenses'),
          accRow('techlit-other-expenses',                  'techlit-other-expenses'),

          accRow('kenya-hq-rent-expenses',       'kenya-hq-rent-expenses'),
          accRow('media-studio-setup-expenses',  'media-studio-setup-expenses'),
          accRow('student-scholarship-expenses', 'student-scholarship-expenses'),
          accRow('other-program-expenses',       'other-program-expenses'),

          accRow('mgmt-salaries',                'mgmt-salaries'),
          accRow('mgmt-payroll-taxes',           'mgmt-payroll-taxes'),
          accRow('mgmt-money-fees-expenses',     'mgmt-money-fees-expenses'),
          accRow('mgmt-admin-services-expenses', 'mgmt-admin-services-expenses'),
          accRow('mgmt-cloud-services-expenses', 'mgmt-cloud-services-expenses'),
          accRow('mgmt-other-expenses',          'mgmt-other-expenses'),

          accRow('fundraising-salaries',             'fundraising-salaries'),
          accRow('fundraising-travel-expenses',      'fundraising-travel-expenses'),
          accRow('fundraising-events-expenses',      'fundraising-events-expenses'),
          accRow('fundraising-marketing-expenses',   'fundraising-marketing-expenses'),
          accRow('fundraising-advertising-expenses', 'fundraising-advertising-expenses'),
          accRow('fundraising-other-expenses',       'fundraising-other-expenses'),
        ], 'Total Expenses'),

        row('assets', 'Assets', [
          accRow('checking-general', 'checking-general'),
          accRow('checking-admin',   'checking-admin'),
          accRow('paypal-cash',      'paypal-cash'),

          accRow('grants-receivable',         'grants-receivable'),
          accRow('stripe-receivables',        'stripe-receivables'),
          accRow('paypal-giving-receivables', 'paypal-giving-receivables'),

          accRow('equipment-assets',       'equipment-assets'),
          accRow('kenya-hq-prepaid-lease', 'kenya-hq-prepaid-lease'),
        ], 'Total Assets'),

        row('liabilities', 'Liabilities', [
          accRow('gusto-payable',             'gusto-payable'),
          accRow('techlit-providers-payable', 'techlit-providers-payable'),
          accRow('reimbursable-expenses',     'reimbursable-expenses'),
          accRow('other-payables',            'other-payables'),
        ], 'Total Liabilities'),

        row('equity', 'Equity', [
          accRow('general-net-assets',              'general-net-assets'),
          accRow('admin-net-assets',                'admin-net-assets'),
          accRow('iex-schools-net-assets',          'iex-schools-net-assets'),
          accRow('chepyuan-school-net-assets',      'chepyuan-school-net-assets'),
          accRow('logiri-school-net-assets',        'logiri-school-net-assets'),
          accRow('shipping-net-assets',             'shipping-net-assets'),
          accRow('kenya-hq-net-assets',             'kenya-hq-net-assets'),
          accRow('media-studio-net-assets',         'media-studio-net-assets'),
          accRow('student-scholarships-net-assets', 'student-scholarships-net-assets'),
        ], 'Total Equity'),
      ], 'Sum'),
    ],

    cols: [
      col('opening', [t('Opening', 'h-28 font-bold center')], [
        col('opening-dr', [t('Dr.', 'h-28 font-bold center')]),
        col('opening-cr', [t('Cr.', 'h-28 font-bold center')]),
      ]),

      col('activity', [t('Activity', 'h-28 font-bold center')], [
        col('activity-dr', [t('Dr.', 'h-28 font-bold center')]),
        col('activity-cr', [t('Cr.', 'h-28 font-bold center')]),
      ]),

      col('adjustments', [t('Adjustments', 'h-28 font-bold center')], [
        col('adjustments-dr', [t('Dr.', 'h-28 font-bold center')]),
        col('adjustments-cr', [t('Cr.', 'h-28 font-bold center')]),
      ]),

      col('closing', [t('Closing', 'h-28 font-bold center')], [
        col('closing-dr', [t('Dr.', 'h-28 font-bold center')]),
        col('closing-cr', [t('Cr.', 'h-28 font-bold center')]),
      ]),
    ],

    mapData: ({cells, accountsLookup}, {date, acc, dr, cr}) => {
      if (date > period.end) return
      const {nat_dr, temp} = accountsLookup[acc]

      if (date < period.start) {
        if (temp) return
        if (nat_dr) cells['opening-dr'][acc] += dr - cr
        else cells['opening-cr'][acc] += cr - dr
      } else if (acc.includes('net-assets')) {
        if (nat_dr) cells['adjustments-dr'][acc] += dr - cr
        else cells['adjustments-cr'][acc] += cr - dr
      } else {
        if (nat_dr) cells['activity-dr'][acc] += dr - cr
        else cells['activity-cr'][acc] += cr - dr
      }
    },

    colSums: {
      'closing-dr': (ctx, colId, rowId) =>
        ctx.accountsLookup[rowId]?.temp
          ? 0
          : colSum('opening-dr', 'activity-dr', 'adjustments-dr')(ctx, colId, rowId),

      'closing-cr': (ctx, colId, rowId) =>
        ctx.accountsLookup[rowId]?.temp
          ? 0
          : colSum('opening-cr', 'activity-cr', 'adjustments-cr')(ctx, colId, rowId),
    },
  })

export const Us2023ActivityByMonth = ({periodId}) => pug`
  - const period = getUs2023Period(periodId)
  - const nextPeriodId = nextUs2023Period(period.id)
  - const prevPeriodId = prevUs2023Period(period.id)

  Report(report=us2023ActivityByMonth(period) fmt='usd')
    .h-full.col
      .text-3xl.font-bold Trial Balance
      .row.gap-4.justify-between.pt-3
        = (prevPeriodId ? $nav : $disabled)('/us2023/trial-balance/'+prevPeriodId, 'Prev')
        .text-lg.font-bold #{date(period.start)} - #{date(period.end)}
        = (nextPeriodId ? $nav : $disabled)('/us2023/trial-balance/'+nextPeriodId, 'Next')
`
export default Us2023ActivityByMonth
