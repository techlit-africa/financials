import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'
import {Us2023Layout} from '@/layouts/Us2023Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date, titleize} from '@/lib'
import {dateToUs2023Period, getUs2023Period, nextUs2023Period, prevUs2023Period, us2023} from '@/journals/us2023'

const us2023Reimbursements = (parties, period) => {
  const employees = []
  parties.forEach(({name, kind}) => {
    if (kind != 'employee') return
    employees.push(name)
  })
  
  return createReport({
    journalSignal: us2023,

    rows: [
      row('employees', 'Employees', [
        ...employees.map((name) => row(name, name)),
      ], 'Total'),
    ],

    cols: [
      col('summary', [], [
        col('opening', [t(`Before ${date(period.start)}`, 'h-32 font-bold justify-center'),]),
        {...col('received', [t('Received', 'h-32 font-bold justify-center'),]), type: 'change'},
        {...col('expensed', [t('Expensed', 'h-32 font-bold justify-center'),]), type: 'change'},
      ]),
      col('closing', [], [], [t(`As of ${date(period.end)}`, 'h-32 font-bold justify-center',)]),
    ],

    mapData: ({accountsLookup, cells}, {date, acc, party, dr, cr}) => {
      if (date > period.end) return
      if (!party || acc != 'reimbursable-expenses') return

      cells['closing'][party] += cr - dr

      if (date < period.start) {
        cells['opening'][party] += cr - dr
      } else {
        if (dr) cells['received'][party] += dr
        if (cr) cells['expensed'][party] -= cr
      }
    },
  })
}

export const Us2023Reimbursements = ({periodId}) => pug`
  - const period = getUs2023Period(periodId)
  - const prevPeriodId = prevUs2023Period(period.id)
  - const nextPeriodId = nextUs2023Period(period.id)

  Report(report=us2023Reimbursements(us2023.value.parties, period) fmt='usd')
    .text-3xl.pb-4.font-bold Reimbursements
    .row.gap-x-4.flex-wrap
      = $nav('/us2023/cash-balances/'+periodId, 'Cash Balances')
      = $nav('/us2023/top-channels/'+periodId, 'Top Channels')
      = $nav('/us2023/top-donors/'+periodId, 'Top Donors')
      = $nav('/us2023/salaries/'+periodId, 'Salaries')
      = $active('/us2023/reimbursements/'+periodId, 'Reimbursements')
      = $nav('/us2023/kenya-payables/'+periodId, 'Kenya Payables')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2023/reimbursements/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2023/reimbursements/'+nextPeriodId, 'Next')
`
export default Us2023Reimbursements
