import {useEffect} from 'preact/hooks'
import {Us2023Layout} from '@/layouts/Us2023Layout'
import {us2023Entries} from '@/journals/us2023'
import {Entry, Line, $tag} from '@/comps/Journal'

export const Us2023Journal = () => pug`
  -
    useEffect(() => {
      const hash = window.location.hash
      window.location.hash = ''
      window.location.hash = hash
    }, [us2023Entries.value])

  .w-full.min-w-screen-md.flex-1.overflow-y-scroll.smooth-scroll
    for entry in us2023Entries.value
      Entry(key=entry.id entry=entry fmt='usd')
        for line in entry.lines
          Line(key=line.id line=line fmt='usd')
            if line.fund != 'general'
              = $tag('$', line.fund, 'bg-pink-100')
            = $tag('@', line.party, 'bg-blue-100')
            = $tag('#', line.channel, 'bg-orange-100')
`
export default Us2023Journal
