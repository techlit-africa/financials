import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'
import {Us2023Layout} from '@/layouts/Us2023Layout'
import {$active, $disabled, $nav} from '@/comps/Macros'
import {date, titleize} from '@/lib'
import {dateToUs2023Period, getUs2023Period, nextUs2023Period, prevUs2023Period, us2023} from '@/journals/us2023'

const us2023TopDonors = (parties, period) => {
  let kinds = {}
  const kindByName = {}
  const donors = []
  parties.forEach(({name, kind}) => {
    if (kind == 'employee') return
    donors.push(name)
    kinds[kind] = true
    kindByName[name] = kind
  })
  kinds = Object.keys(kinds)
  
  return createReport({
    journalSignal: us2023,

    rows: [
      row('total', 'Summary', [
        ...kinds.map((kind) => row(kind, titleize(`${kind} Donations`))),
      ], 'Total Donations'),

      row('all', 'All Donors', [
        ...donors.map((name) => row(name, name)),
      ]),
    ],

    rowSorter: (cells, a, b) => cells['closing'][b.id] - cells['closing'][a.id],

    rowFilter: (cells, row) => row.id == 'all' || cells['closing'][row.id] > 0,

    cols: [
      col('all', [], [
        {type: 'change', ...col('current', [t(`${date(period.start)} - ${date(period.end)}`, 'center')], [
          {type: 'change', ...col('monetary-donations', [t('Monetary', 'font-bold center')])},
          {type: 'change', ...col('equipment-donations', [t('Equipment', 'font-bold center')])},
        ], [t('Total', 'font-bold center')])},
  
        col('closing', [t(`2023 Total`, 'center')], [
          col('closing-monetary-donations', [t('Monetary', 'font-bold center')]),
          col('closing-equipment-donations', [t('Equipment', 'font-bold center')]),
        ], [t('Total', 'font-bold center')]),
      ])
    ],

    mapData: ({accountsLookup, cells}, {date, acc, party, dr, cr}) => {
      if (date > period.end) return
      if (!party || (acc != 'monetary-donations' && acc != 'equipment-donations')) return

      const kind = kindByName[party]
      if (!kind || kind == 'employee') return

      cells[`closing-${acc}`][party] += cr - dr
      cells[`closing-${acc}`][kind] += cr - dr

      if (date >= period.start) {
        cells[acc][party] += cr - dr
        cells[acc][kind] += cr - dr
      }
    },
  })
}

export const Us2023TopDonors = ({periodId}) => pug`
  - const period = getUs2023Period(periodId)
  - const prevPeriodId = prevUs2023Period(period.id)
  - const nextPeriodId = nextUs2023Period(period.id)

  Report(report=us2023TopDonors(us2023.value.parties, period) fmt='usd')
    .text-3xl.pb-4.font-bold Top Donors
    .row.gap-x-4.flex-wrap
      = $nav('/us2023/cash-balances/'+periodId, 'Cash Balances')
      = $nav('/us2023/top-channels/'+periodId, 'Top Channels')
      = $active('/us2023/top-donors/'+periodId, 'Top Donors')
      = $nav('/us2023/salaries/'+periodId, 'Salaries')
      = $nav('/us2023/reimbursements/'+periodId, 'Reimbursements')
      = $nav('/us2023/kenya-payables/'+periodId, 'Kenya Payables')
    .row.gap-4.justify-between.pt-3
      = (prevPeriodId ? $nav : $disabled)('/us2023/top-donors/'+prevPeriodId, 'Prev')
      .text-lg.font-bold As of #{date(period.end)}
      = (nextPeriodId ? $nav : $disabled)('/us2023/top-donors/'+nextPeriodId, 'Next')
`
export default Us2023TopDonors
