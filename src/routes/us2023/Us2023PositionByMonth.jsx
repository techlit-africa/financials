import {Us2023Layout} from '@/layouts/Us2023Layout'
import {Report, row, accRow, col, t, createReport, rowSum} from '@/comps/Report'

import {us2023, us2023Periods, prevUs2023Period, dateToUs2023Period} from '@/journals/us2023'
import {$active, $nav} from '@/comps/Macros'

//
// Rows that have an ID different from their name
//
const rowIdForAccountName = (name) =>
  ({
    'general-net-assets': 'unrestricted',

    'iex-schools-net-assets': 'schools-net-assets',
    'chepyuan-school-net-assets': 'schools-net-assets',
    'logiri-school-net-assets': 'schools-net-assets',

    'shipping-net-assets': 'other-net-assets',
    'kenya-hq-net-assets': 'other-net-assets',
    'media-studio-net-assets': 'other-net-assets',
    'student-scholarships-net-assets': 'other-net-assets',
  })[name] || name

const us2023PositionByMonth = () =>
  createReport({
    journalSignal: us2023,

    isCumulative: true,

    rows: [
      row('assets', 'Assets', [
        row('cash', 'Cash', [
          accRow('checking-general', 'Checking - General'),
          accRow('checking-admin',   'Checking - Admin'),
          accRow('paypal-cash',      'PayPal Account'),
        ], 'Total Cash'),

        row('receivables', 'Receivables', [
          accRow('grants-receivable',         'Grants Receivable'),
          accRow('stripe-receivables',        'Stripe Receivables'),
          accRow('paypal-giving-receivables', 'PayPal Giving'),
        ], 'Total Receivables'),

        row('fixed', 'Fixed Assets', [
          accRow('equipment-assets',       'Computers & Accessories'),
          accRow('kenya-hq-prepaid-lease', 'Kenya HQ Prepaid Lease'),
        ], 'Total Fixed Assets'),
      ], 'Total Assets'),

      row('lna', 'Liabilities Net Assets', [
        row('liabilities', 'Liabilities', [
          accRow('gusto-payable',             'Accrued Payroll'),
          accRow('techlit-providers-payable', 'TechLit Kenya Payables'),
          accRow('reimbursable-expenses',     'Reimbursable Expenses'),
          // accRow('other-payables',            'Other Payables'),
        ], 'Total Liabilities'),

        row('net-assets', 'Net Assets', [
          accRow('unrestricted', 'Unrestricted'),
          row('restricted',      'Restricted'),

          row('res', 'Restrictions', [
            accRow('admin-net-assets',   'Admin'),
            accRow('schools-net-assets', 'Schools'),
            accRow('other-net-assets',   'Other'),
          ]),

          // row('schools-breakdown', 'Schools', [
          //   accRow('iex-schools-net-assets', 'IEX Schools'),
          //   accRow('chepyuan-school-net-assets', 'Chepyuan School'),
          //   accRow('logiri-school-net-assets', 'Logiri School'),
          // ]),
          // row('other-breakdown', 'Other', [
          //   // accRow('shipping-net-assets', 'Shipping & Customs'),
          //   accRow('kenya-hq-net-assets', 'Kenya HQ'),
          //   accRow('media-studio-net-assets', 'Media Studio'),
          //   accRow('student-scholarships-net-assets', 'Student Scholarships'),
          // ]),

          {...row('net-assets-change', 'Change in Net Assets', [
            {...row('unrestricted-change', 'Unrestricted'), type: 'change'},
            {...row('restricted-change', 'Restricted'), type: 'change'},
          ], 'Total Change in Net Assets'),
            type: 'change'},
        ], 'Total Net Assets'),
      ], 'Total Liabilities & Net Assets'),
    ],

    cols: [
      col('2022', [], [
        col('2022-12', [
          t('EOY', 'text-sm text-gray-400 center'),
          t('2022', 'justify-end'),
        ]),
      ]),
      col('2023-breakdown', [], [
        ...us2023Periods
        .filter(({id}) => id != '2023')
        // .filter(({end}) => end <= us2023.value.settings.latest)
        .map(({id, name}) =>
          col(id, [
            t('As of', 'h-4 text-sm text-gray-400 justify-center'),
            t(name, 'justify-end'),
          ]),
        ),
      ]),
      col('2023', [], [
      ], [t('EOY', 'text-base opacity-50 center'), t('2023', 'justify-end')]),
    ],

    rowSums: {
      'restricted': rowSum(
        'admin-net-assets',
        'schools-net-assets',
        'other-net-assets',
      ),

      'net-assets': rowSum('restricted', 'unrestricted'),

      'lna': ({cells}, colId, rowId) => {
        const col = cells[colId]
        col[rowId] = col['liabilities'] + col['net-assets']
      },

      'net-assets-change': ({cells}, colId, rowId) => {
        const curr = cells[colId]

        if (colId == '2022-12' || colId == '2022' || colId == '2023') {
          curr.change = 0
        } else {
          const prevId = colId == '2023-01' ? '2022-12' : prevUs2023Period(colId)
          const prev = cells[prevId]

          curr['unrestricted-change'] = curr['unrestricted'] - prev['unrestricted']
          curr['restricted-change'] = curr['restricted'] - prev['restricted']
          curr[rowId] = curr['net-assets'] - prev['net-assets']
        }
      },
    },

    mapData: ({accountsLookup, cells}, {date, acc, dr, cr}) => {
      const colId = dateToUs2023Period(date)
      const account = accountsLookup[acc]
      const rowId = rowIdForAccountName(acc)
      cells[colId][rowId] += account.nat_dr ? dr - cr : cr - dr
    },
  })

export const Us2023PositionByMonth = () => pug`
  Report(report=us2023PositionByMonth() fmt='usd')
    .text-3xl.font-bold Statement of Position
    .row.flex-wrap.gap-x-4
      = $active('/us2023/position-by-month', 'Monthly')
      = $nav('/us2023/position-by-fund/latest', 'Restricted')
      = $nav('/us2023/cash-balances/latest', 'Cash Position')
      a.text-base.text-gray-500.hover_text-blue-700.hover_underline(href='/us2023/activity-by-month') (Activity)
`
export default Us2023PositionByMonth
