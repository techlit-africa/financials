import {useEffect} from 'preact/hooks'

const dateFormatter = new Intl.DateTimeFormat('en-US', {month: 'short', day: 'numeric', timeZone: 'UTC'})
export const date = (dateStr) => {
  const d = dateFormatter.format(Date.parse(dateStr))
  return dateStr > '2022-12-31' ? d : `${d}, 2022`
}

const kesFormatter = new Intl.NumberFormat('en-US', {maximumFractionDigits: 0, currencySign: 'accounting'})
export const kes = (kesStr) => {
  const n = kesFormatter.format(kesStr)
  if (n == 0) return '-'
  return n[0] == '-' ? `(${n.slice(1)})` : n
}

const usdFormatter = new Intl.NumberFormat('en-US', {roundingIncrement: 1, minimumFractionDigits: 2, currencySign: 'accounting'})
export const usd = (usdStr) => {
  const n = usdFormatter.format(usdStr)
  if (n == 0) return '-'
  return n[0] == '-' ? `(${n.slice(1)})` : n
}

export const onMount = (fn) => useEffect(() => { fn() }, [])

export const fetchJson = async (...xs) => {
  const response = await fetch(...xs)
  return await response.json()
}

export const titleize = (s) =>
  s.split(/[-_ ']+/)
    .map((w) => `${w[0]?.toUpperCase()}${w.slice(1)}`)
    .join(' ')
    .replaceAll('And', '&')
