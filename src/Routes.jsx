import {Router} from "preact-router";

import Index from "@/routes/index";

import Us2023Layout from "@/layouts/Us2023Layout";

import Us2023Index from "@/routes/us2023/Us2023Index";
import Us2023Journal from "@/routes/us2023/Us2023Journal";

import Us2023Accounts from "@/routes/us2023/Us2023Accounts";
import Us2023Account from "@/routes/us2023/Us2023Account";

import Us2023PositionByMonth from "@/routes/us2023/Us2023PositionByMonth";
import Us2023PositionByFund from "@/routes/us2023/Us2023PositionByFund";

import Us2023ActivityByMonth from "@/routes/us2023/Us2023ActivityByMonth";
import Us2023ActivityByFund from "@/routes/us2023/Us2023ActivityByFund";

import Us2023TrialBalance from "@/routes/us2023/Us2023TrialBalance";
import Us2023CashBalances from "@/routes/us2023/Us2023CashBalances";
import Us2023TopChannels from "@/routes/us2023/Us2023TopChannels";
import Us2023TopDonors from "@/routes/us2023/Us2023TopDonors";
import Us2023SalaryByFunction from "@/routes/us2023/Us2023SalaryByFunction";
import Us2023Reimbursements from "@/routes/us2023/Us2023Reimbursements";
import Us2023KenyaPayabless from "@/routes/us2023/Us2023KenyaPayables";

const Us2023Routes = () => pug`
  Us2023Layout
    Router
      Us2023Index(path='/us2023')
      Us2023Journal(path='/us2023/journal')

      Us2023Accounts(path='/us2023/accounts')
      Us2023Account(path='/us2023/accounts/:name')

      Us2023PositionByMonth(path='/us2023/position-by-month')
      Us2023ActivityByMonth(path='/us2023/activity-by-month')

      Us2023PositionByFund(path='/us2023/position-by-fund/:periodId')
      Us2023ActivityByFund(path='/us2023/activity-by-fund/:periodId')

      Us2023TrialBalance(path='/us2023/trial-balance/:periodId')
      Us2023CashBalances(path='/us2023/cash-balances/:periodId')
      Us2023TopChannels(path='/us2023/top-channels/:periodId')
      Us2023TopDonors(path='/us2023/top-donors/:periodId')
      Us2023SalaryByFunction(path='/us2023/salaries/:periodId')
      Us2023Reimbursements(path='/us2023/reimbursements/:periodId')
      Us2023KenyaPayabless(path='/us2023/kenya-payables/:periodId')
`;

import Us2024Layout from "@/layouts/Us2024Layout";

import Us2024Index from "@/routes/us2024/Us2024Index";
import Us2024Journal from "@/routes/us2024/Us2024Journal";

import Us2024Accounts from "@/routes/us2024/Us2024Accounts";
import Us2024Account from "@/routes/us2024/Us2024Account";

import Us2024PositionByMonth from "@/routes/us2024/Us2024PositionByMonth";
import Us2024PositionByFund from "@/routes/us2024/Us2024PositionByFund";

import Us2024ActivityByMonth from "@/routes/us2024/Us2024ActivityByMonth";
import Us2024ActivityByFund from "@/routes/us2024/Us2024ActivityByFund";

import Us2024CashBalances from "@/routes/us2024/Us2024CashBalances";
import Us2024TopChannels from "@/routes/us2024/Us2024TopChannels";
import Us2024TopDonors from "@/routes/us2024/Us2024TopDonors";
import Us2024SalaryByFunction from "@/routes/us2024/Us2024SalaryByFunction";
import Us2024Reimbursements from "@/routes/us2024/Us2024Reimbursements";
import Us2024KenyaPayabless from "@/routes/us2024/Us2024KenyaPayables";

const Us2024Routes = () => pug`
  Us2024Layout
    Router
      Us2024Index(path='/us2024' default)
      Us2024Journal(path='/us2024/journal')

      Us2024Accounts(path='/us2024/accounts')
      Us2024Account(path='/us2024/accounts/:name')

      Us2024PositionByMonth(path='/us2024/position-by-month')
      Us2024ActivityByMonth(path='/us2024/activity-by-month')

      Us2024PositionByFund(path='/us2024/position-by-fund/:periodId')
      Us2024ActivityByFund(path='/us2024/activity-by-fund/:periodId')

      Us2024CashBalances(path='/us2024/cash-balances/:periodId')
      Us2024TopChannels(path='/us2024/top-channels/:periodId')
      Us2024TopDonors(path='/us2024/top-donors/:periodId')
      Us2024SalaryByFunction(path='/us2024/salaries/:periodId')
      Us2024Reimbursements(path='/us2024/reimbursements/:periodId')
      Us2024KenyaPayabless(path='/us2024/kenya-payables/:periodId')
`;

import Ke2023Layout from "@/layouts/Ke2023Layout";

import Ke2023Index from "@/routes/ke2023/Ke2023Index";
import Ke2023Journal from "@/routes/ke2023/Ke2023Journal";

import Ke2023Accounts from "@/routes/ke2023/Ke2023Accounts";
import Ke2023Account from "@/routes/ke2023/Ke2023Account";

import Ke2023PositionByWeek from "@/routes/ke2023/Ke2023PositionByWeek";

import Ke2023ActivityByWeek from "@/routes/ke2023/Ke2023ActivityByWeek";
import Ke2023ActivityBySchool from "@/routes/ke2023/Ke2023ActivityBySchool";

import Ke2023PettyCash from "@/routes/ke2023/Ke2023PettyCash";
import Ke2023RentPayables from "@/routes/ke2023/Ke2023RentPayables";
import Ke2023VendorPayables from "@/routes/ke2023/Ke2023VendorPayables";
import Ke2023Stipends from "@/routes/ke2023/Ke2023Stipends";
import Ke2023SchoolReceivables from "@/routes/ke2023/Ke2023SchoolReceivables";

const Ke2023Routes = () => pug`
  Ke2023Layout
    Router
      Ke2023Index(path='/ke2023' default)
      Ke2023Journal(path='/ke2023/journal')

      Ke2023Accounts(path='/ke2023/accounts')
      Ke2023Account(path='/ke2023/accounts/:name')

      Ke2023PositionByWeek(path='/ke2023/position-by-week')

      Ke2023ActivityByWeek(path='/ke2023/activity-by-week')
      Ke2023ActivityBySchool(path='/ke2023/activity-by-school/:periodId')

      Ke2023Stipends(path='/ke2023/stipends/:periodId')
      Ke2023PettyCash(path='/ke2023/petty-cash/:periodId')
      Ke2023RentPayables(path='/ke2023/rent-payables/:periodId')
      Ke2023VendorPayables(path='/ke2023/vendor-payables/:periodId')
      Ke2023SchoolReceivables(path='/ke2023/school-receivables/:periodId')
`;

import Ke2024Layout from "@/layouts/Ke2024Layout";

import Ke2024Index from "@/routes/ke2024/Ke2024Index";
import Ke2024Journal from "@/routes/ke2024/Ke2024Journal";

import Ke2024Accounts from "@/routes/ke2024/Ke2024Accounts";
import Ke2024Account from "@/routes/ke2024/Ke2024Account";

import Ke2024PositionByMonth from "@/routes/ke2024/Ke2024PositionByMonth";

import Ke2024ActivityByMonth from "@/routes/ke2024/Ke2024ActivityByMonth";
import Ke2024ActivityBySchool from "@/routes/ke2024/Ke2024ActivityBySchool";

import Ke2024PettyCash from "@/routes/ke2024/Ke2024PettyCash";
import Ke2024RentPayables from "@/routes/ke2024/Ke2024RentPayables";
import Ke2024VendorPayables from "@/routes/ke2024/Ke2024VendorPayables";
import Ke2024Stipends from "@/routes/ke2024/Ke2024Stipends";
import Ke2024SchoolReceivables from "@/routes/ke2024/Ke2024SchoolReceivables";

const Ke2024Routes = () => pug`
  Ke2024Layout
    Router
      Ke2024Index(path='/ke2024' default)
      Ke2024Journal(path='/ke2024/journal')

      Ke2024Accounts(path='/ke2024/accounts')
      Ke2024Account(path='/ke2024/accounts/:name')

      Ke2024PositionByMonth(path='/ke2024/position-by-month')

      Ke2024ActivityByMonth(path='/ke2024/activity-by-month')
      Ke2024ActivityBySchool(path='/ke2024/activity-by-school/:periodId')

      Ke2024Stipends(path='/ke2024/stipends/:periodId')
      Ke2024PettyCash(path='/ke2024/petty-cash/:periodId')
      Ke2024RentPayables(path='/ke2024/rent-payables/:periodId')
      Ke2024VendorPayables(path='/ke2024/vendor-payables/:periodId')
      Ke2024SchoolReceivables(path='/ke2024/school-receivables/:periodId')
`;

export const Routes = () => pug`
  Router
    Index(path='/' default)

    Us2023Routes(path='/us2023/:rest*')
    Ke2023Routes(path='/ke2023/:rest*')
    //Us2024Routes(path='/us2024/:rest*')
    //Ke2024Routes(path='/ke2024/:rest*')
`;
