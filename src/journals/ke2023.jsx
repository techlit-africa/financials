import {batch, computed, signal} from '@preact/signals'
import {fetchJson, date} from '@/lib'

export const ke2023 = signal(null)
export const ke2023Failed = signal(true)
export const ke2023IsLoading = signal(true)

export const loadKe2023 = async (refresh = false) => {
  try {
    ke2023Failed.value = false
    ke2023IsLoading.value = true

    const apiUrl = import.meta.env.VITE_API_URL
    const query = refresh ? '?refresh=true' : ''
    const value = await fetchJson(`${apiUrl}/api/v1/books2023/ke${query}`)
  
    batch(() => {
      ke2023Failed.value = false
      ke2023IsLoading.value = false
      ke2023.value = value
    })
  } catch (e) {
    ke2023Failed.value = e
  }
}

export const dateToKe2023Period = (dateStr) => {
  if (dateStr < '2023-01-01') return '-1'
  if (dateStr > '2023-12-31') return '2023'
  if (dateStr > '2023-12-29') return '51'

  const date = new Date(dateStr)
  const firstSaturday = new Date(date.getFullYear(), 0, 7)
  const daysSinceFirstSaturday = Math.floor(
    (date - firstSaturday) / (24 * 60 * 60 * 1000),
  )
  return (1 + Math.floor(daysSinceFirstSaturday / 7)).toString()
}

const period = (start, end) => ({
  start,
  end,
  name: date(end),
  id: dateToKe2023Period(end),
})
export const ke2023Periods = [
  period('2023-01-01', '2023-01-06'),
  period('2023-01-07', '2023-01-13'),
  period('2023-01-14', '2023-01-20'),
  period('2023-01-21', '2023-01-27'),
  period('2023-01-28', '2023-02-03'),
  period('2023-02-04', '2023-02-10'),
  period('2023-02-11', '2023-02-17'),
  period('2023-02-18', '2023-02-24'),
  period('2023-02-25', '2023-03-03'),
  period('2023-03-04', '2023-03-10'),
  period('2023-03-11', '2023-03-17'),
  period('2023-03-18', '2023-03-24'),
  period('2023-03-25', '2023-03-31'),
  period('2023-04-01', '2023-04-07'),
  period('2023-04-08', '2023-04-14'),
  period('2023-04-15', '2023-04-21'),
  period('2023-04-22', '2023-04-28'),
  period('2023-04-29', '2023-05-05'),
  period('2023-05-06', '2023-05-12'),
  period('2023-05-13', '2023-05-19'),
  period('2023-05-20', '2023-05-26'),
  period('2023-05-27', '2023-06-02'),
  period('2023-06-03', '2023-06-09'),
  period('2023-06-10', '2023-06-16'),
  period('2023-06-17', '2023-06-23'),
  period('2023-06-24', '2023-06-30'),
  period('2023-07-01', '2023-07-07'),
  period('2023-07-08', '2023-07-14'),
  period('2023-07-15', '2023-07-21'),
  period('2023-07-22', '2023-07-28'),
  period('2023-07-29', '2023-08-04'),
  period('2023-08-05', '2023-08-11'),
  period('2023-08-12', '2023-08-18'),
  period('2023-08-19', '2023-08-25'),
  period('2023-08-26', '2023-09-01'),
  period('2023-09-02', '2023-09-08'),
  period('2023-09-09', '2023-09-15'),
  period('2023-09-16', '2023-09-22'),
  period('2023-09-23', '2023-09-29'),
  period('2023-09-30', '2023-10-06'),
  period('2023-10-07', '2023-10-13'),
  period('2023-10-14', '2023-10-20'),
  period('2023-10-21', '2023-10-27'),
  period('2023-10-28', '2023-11-03'),
  period('2023-11-04', '2023-11-10'),
  period('2023-11-11', '2023-11-17'),
  period('2023-11-18', '2023-11-24'),
  period('2023-11-25', '2023-12-01'),
  period('2023-12-02', '2023-12-08'),
  period('2023-12-09', '2023-12-15'),
  period('2023-12-16', '2023-12-22'),
  period('2023-12-23', '2023-12-31'),
  {...period('2023-01-01', '2023-12-31'), id: '2023'},
]

export const getKe2023Period = (periodId) => {
  if (periodId == 'latest') periodId = '2023'
  // if (periodId == 'latest') periodId = dateToKe2023Period(ke2023.value.settings.latest)
  return ke2023Periods.find(({id}) => id == periodId)
}

export const prevKe2023Period = (periodId) =>
  ke2023Periods[ke2023Periods.findIndex(({id}) => id == periodId) - 1]?.id
export const nextKe2023Period = (periodId) => {
  if (periodId == '2023') return
  if (periodId == dateToKe2023Period(ke2023.value.settings.latest)) return '2023'
  return ke2023Periods[ke2023Periods.findIndex(({id}) => id == periodId) + 1]?.id
}

export const ke2023Entries = computed(() => {
  const {entry_ids, entries, lines} = ke2023.value
  if (!entry_ids) return []

  return entry_ids.map((eid) => {
    const {line_ids = [], ...entry} = entries[eid]
    return {...entry, lines: line_ids.map((lid) => lines[lid])}
  })
})

export const ke2023Accounts = computed(() => {
  const {lines, accounts} = ke2023.value
  if (!accounts.length) return {}

  const byAccount = accounts.reduce((o, {name, nat_dr, temp}) => {
    o[name] = {name, dr: 0, cr: 0, bal: 0, nat_dr, temp, lines: []}
    return o
  }, {})

  Object.values(lines).forEach((line) => {
    const acc = byAccount[line.acc]
    if (!acc) return

    acc.dr += line.dr
    acc.cr += line.cr
    acc.bal += acc.nat_dr ? line.dr - line.cr : line.cr - line.dr
    acc.lines.unshift({...line, bal: acc.bal, nat_dr: acc.nat_dr})
  })

  return byAccount
})
