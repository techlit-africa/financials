import {batch, computed, signal} from '@preact/signals'
import {fetchJson, date} from '@/lib'

export const us2024 = signal(null)
export const us2024Failed = signal(true)
export const us2024IsLoading = signal(true)

export const loadUs2024 = async (refresh = false) => {
  try {
    us2024Failed.value = false
    us2024IsLoading.value = true

    const apiUrl = import.meta.env.VITE_API_URL
    const query = refresh ? '?refresh=true' : ''
    const value = await fetchJson(`${apiUrl}/api/v1/books2024/us${query}`)
  
    batch(() => {
      us2024Failed.value = false
      us2024IsLoading.value = false
      us2024.value = value
    })
  } catch (e) {
    us2024Failed.value = e
  }
}

export const dateToUs2024Period = (dateStr) => {
  if (dateStr > '2024-12-31') return '2024'
  return dateStr.slice(0, 7)
}

const period = (start, end) => ({
  start,
  end,
  name: date(end),
  id: dateToUs2024Period(end),
})
export const us2024Periods = [
  period('2024-01-01', '2024-01-31'),
  period('2024-02-01', '2024-02-28'),
  period('2024-03-01', '2024-03-31'),
  period('2024-04-01', '2024-04-30'),
  period('2024-05-01', '2024-05-31'),
  period('2024-06-01', '2024-06-30'),
  period('2024-07-01', '2024-07-31'),
  period('2024-08-01', '2024-08-31'),
  period('2024-09-01', '2024-09-30'),
  period('2024-10-01', '2024-10-31'),
  period('2024-11-01', '2024-11-30'),
  period('2024-12-01', '2024-12-31'),
  {...period('2024-01-01', '2024-12-31'), id: '2024'},
]

export const getUs2024Period = (periodId) => {
  // if (periodId == 'latest') periodId = '2024'
  if (periodId == 'latest') periodId = dateToUs2024Period(us2024.value.settings.latest)
  return us2024Periods.find(({id}) => id == periodId)
}
export const prevUs2024Period = (periodId) =>
  us2024Periods[us2024Periods.findIndex(({id}) => id == periodId) - 1]?.id
export const nextUs2024Period = (periodId) => {
  if (periodId == '2024') return
  if (periodId == dateToUs2024Period(us2024.value.settings.latest)) return '2024'
  return us2024Periods[us2024Periods.findIndex(({id}) => id == periodId) + 1]?.id
}

export const us2024Entries = computed(() => {
  const {entry_ids, entries, lines} = us2024.value
  if (!entry_ids) return []

  return entry_ids.map((eid) => {
    const {line_ids = [], ...entry} = entries[eid]
    return {...entry, lines: line_ids.map((lid) => lines[lid])}
  })
})

export const us2024Accounts = computed(() => {
  const {line_ids, lines, accounts} = us2024.value
  if (!accounts?.length) return {}

  const byName = accounts.reduce((o, {name, nat_dr, temp}) => {
    o[name] = {name, dr: 0, cr: 0, bal: 0, nat_dr, temp, lines: []}
    return o
  }, {})

  Object.values(lines).forEach((line) => {
    const acc = byName[line.acc]
    if (!acc) return

    acc.dr += line.dr
    acc.cr += line.cr
    acc.bal += acc.nat_dr ? line.dr - line.cr : line.cr - line.dr
    acc.lines.unshift({...line, bal: acc.bal, nat_dr: acc.nat_dr})
  })

  return byName
})
