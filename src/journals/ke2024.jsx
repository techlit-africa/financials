import {batch, computed, signal} from '@preact/signals'
import {fetchJson, date} from '@/lib'

export const ke2024 = signal(null)
export const ke2024Failed = signal(true)
export const ke2024IsLoading = signal(true)

export const loadKe2024 = async (refresh = false) => {
  try {
    ke2024Failed.value = false
    ke2024IsLoading.value = true

    const apiUrl = import.meta.env.VITE_API_URL
    const query = refresh ? '?refresh=true' : ''
    const value = await fetchJson(`${apiUrl}/api/v1/books2024/ke${query}`)
  
    batch(() => {
      ke2024Failed.value = false
      ke2024IsLoading.value = false
      ke2024.value = value
    })
  } catch (e) {
    ke2024Failed.value = e
  }
}

export const dateToKe2024Period = (dateStr) => {
  if (dateStr > '2024-12-31') return '2024'
  return dateStr.slice(0, 7)
}

const period = (start, end) => ({
  start,
  end,
  name: date(end),
  id: dateToKe2024Period(end),
})
export const ke2024Periods = [
  period('2024-01-01', '2024-01-31'),
  period('2024-02-01', '2024-02-29'),
  period('2024-03-01', '2024-03-31'),
  period('2024-04-01', '2024-04-30'),
  period('2024-05-01', '2024-05-31'),
  period('2024-06-01', '2024-06-30'),
  period('2024-07-01', '2024-07-31'),
  period('2024-08-01', '2024-08-31'),
  period('2024-09-01', '2024-09-30'),
  period('2024-10-01', '2024-10-31'),
  period('2024-11-01', '2024-11-30'),
  period('2024-12-01', '2024-12-31'),
  {...period('2024-01-01', '2024-12-31'), id: '2024'},
]

export const getKe2024Period = (periodId) => {
  // if (periodId == 'latest') periodId = '2024'
  if (periodId == 'latest') periodId = dateToKe2024Period(ke2024.value.settings.latest)
  return ke2024Periods.find(({id}) => id == periodId)
}

export const prevKe2024Period = (periodId) =>
  ke2024Periods[ke2024Periods.findIndex(({id}) => id == periodId) - 1]?.id
export const nextKe2024Period = (periodId) => {
  if (periodId == '2024') return
  if (periodId == dateToKe2024Period(ke2024.value.settings.latest)) return '2024'
  return ke2024Periods[ke2024Periods.findIndex(({id}) => id == periodId) + 1]?.id
}

export const ke2024Entries = computed(() => {
  const {entry_ids, entries, lines} = ke2024.value
  if (!entry_ids) return []

  return entry_ids.map((eid) => {
    const {line_ids = [], ...entry} = entries[eid]
    return {...entry, lines: line_ids.map((lid) => lines[lid])}
  })
})

export const ke2024Accounts = computed(() => {
  const {lines, accounts} = ke2024.value
  if (!accounts.length) return {}

  const byAccount = accounts.reduce((o, {name, nat_dr, temp}) => {
    o[name] = {name, dr: 0, cr: 0, bal: 0, nat_dr, temp, lines: []}
    return o
  }, {})

  Object.values(lines).forEach((line) => {
    const acc = byAccount[line.acc]
    if (!acc) return

    acc.dr += line.dr
    acc.cr += line.cr
    acc.bal += acc.nat_dr ? line.dr - line.cr : line.cr - line.dr
    acc.lines.unshift({...line, bal: acc.bal, nat_dr: acc.nat_dr})
  })

  return byAccount
})
