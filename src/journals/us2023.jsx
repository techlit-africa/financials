import {batch, computed, signal} from '@preact/signals'
import {fetchJson, date} from '@/lib'

export const us2023 = signal(null)
export const us2023Failed = signal(true)
export const us2023IsLoading = signal(true)

export const loadUs2023 = async (refresh = false) => {
  try {
    us2023Failed.value = false
    us2023IsLoading.value = true

    const apiUrl = import.meta.env.VITE_API_URL
    const query = refresh ? '?refresh=true' : ''
    const value = await fetchJson(`${apiUrl}/api/v1/books2023/us${query}`)
  
    batch(() => {
      us2023Failed.value = false
      us2023IsLoading.value = false
      us2023.value = value
    })
  } catch (e) {
    us2023Failed.value = e
  }
}

export const dateToUs2023Period = (dateStr) => {
  if (dateStr > '2023-12-31') return '2023'
  return dateStr.slice(0, 7)
}

const period = (start, end) => ({
  start,
  end,
  name: date(end),
  id: dateToUs2023Period(end),
})
export const us2023Periods = [
  period('2023-01-01', '2023-01-31'),
  period('2023-02-01', '2023-02-28'),
  period('2023-03-01', '2023-03-31'),
  period('2023-04-01', '2023-04-30'),
  period('2023-05-01', '2023-05-31'),
  period('2023-06-01', '2023-06-30'),
  period('2023-07-01', '2023-07-31'),
  period('2023-08-01', '2023-08-31'),
  period('2023-09-01', '2023-09-30'),
  period('2023-10-01', '2023-10-31'),
  period('2023-11-01', '2023-11-30'),
  period('2023-12-01', '2023-12-31'),
  {...period('2023-01-01', '2023-12-31'), id: '2023'},
]

export const getUs2023Period = (periodId) => {
  if (periodId == 'latest') periodId = '2023'
  // if (periodId == 'latest') periodId = dateToUs2023Period(us2023.value.settings.latest)
  return us2023Periods.find(({id}) => id == periodId)
}
export const prevUs2023Period = (periodId) =>
  us2023Periods[us2023Periods.findIndex(({id}) => id == periodId) - 1]?.id
export const nextUs2023Period = (periodId) => {
  if (periodId == '2023') return
  if (periodId == dateToUs2023Period(us2023.value.settings.latest)) return '2023'
  return us2023Periods[us2023Periods.findIndex(({id}) => id == periodId) + 1]?.id
}

export const us2023Entries = computed(() => {
  const {entry_ids, entries, lines} = us2023.value
  if (!entry_ids) return []

  return entry_ids.map((eid) => {
    const {line_ids = [], ...entry} = entries[eid]
    return {...entry, lines: line_ids.map((lid) => lines[lid])}
  })
})

export const us2023Accounts = computed(() => {
  const {line_ids, lines, accounts} = us2023.value
  if (!accounts?.length || !line_ids?.length) return {}

  const byName = accounts.reduce((o, {name, nat_dr, temp}) => {
    o[name] = {name, dr: 0, cr: 0, bal: 0, nat_dr, temp, lines: []}
    return o
  }, {})

  Object.values(lines).forEach((line) => {
    const acc = byName[line.acc]
    if (!acc) return

    acc.dr += line.dr
    acc.cr += line.cr
    acc.bal += acc.nat_dr ? line.dr - line.cr : line.cr - line.dr
    acc.lines.unshift({...line, bal: acc.bal, nat_dr: acc.nat_dr})
  })

  return byName
})
