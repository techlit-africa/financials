export const Loading = () => pug`
  .flex-1.w-full.flex.items-center.justify-center
    .h-80.animate.animate-bounce
      .w-20.h-20.grid.grid-cols-3.bg-yellow-100.font-mono.animate.animate-spin
        .center -
        .center -
        .center -
        .center.bg-yellow-300 -
        .center.bg-white -
        .center -
        .center.bg-yellow-300 -
        .center.bg-yellow-300 -
        .center.bg-yellow-300 -
`

export const Failed = ({message}) => pug`
  .flex-1.max-w-sm.mx-auto.flex.flex-col.items-center.justify-center
    .w-12.h-12.center.animate.animate-pulse.bg-red-500.text-4xl.font-bold.text-white !
    .text-red-800= message
`