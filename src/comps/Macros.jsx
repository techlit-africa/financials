export const $nav = (href, title) => pug`
  a.row.items-center.text-xl.font-bold(href=href) [ #[.px-1.text-base.text-blue-700.hover_underline= title] ]
`

export const $active = (href, title) => pug`
  a.row.items-center.text-xl.font-bold(href=href) [ #[.px-1.text-base.bg-yellow-300= title] ]
`

export const $disabled = (href, title) => pug`
  .row.items-center.text-xl.font-bold.opacity-50 [ #[.px-1.text-base.text-gray-600= title] ]
`

export const $btn = (onClick, title) => pug`
  button.row.items-center.gap-1.text-xl.font-bold(onClick=onClick) [ #[.text-base.text-blue-700.hover_underline= title] ]
`