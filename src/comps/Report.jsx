import {useEffect, useRef} from 'preact/hooks'
import {computed} from '@preact/signals'
import {Loading} from '@/comps/Suspense'
import {date, usd, kes} from '@/lib'

const fmtLookup = {kes, usd}

export const row = (id, title, items, summary, href) =>
  ({id, title, items, summary, href})

const absHref = (acc) => `/${window.location.pathname.split('/')[1]}/accounts/${acc}`

export const accRow = (acc, title, items, summary) =>
  row(acc, title, items, summary, absHref(acc))

export const col = row
export const t = (content, clx = '') => ({content, clx})

export const rowSum = (...rowIds) => ({cells}, colId, rowId) => {
  cells[colId][rowId] = rowIds.reduce((sum, id) => sum + cells[colId][id], 0)
}

export const colSum = (...colIds) => ({cells}, colId, rowId) => {
  cells[colId][rowId] = colIds.reduce((sum, id) => sum + cells[id][rowId], 0)
}

const applyRowColFns = (items, cells, sorter, filter) =>
  items
    .map((item) => {
      if (item.items) item.items = applyRowColFns(item.items, cells, sorter, filter)
      return item
    })
    .filter((x) => filter(cells, x))
    .sort((a, b) => sorter(cells, a, b))

//
// Create a report with
// declarate rows & cols and
// imperative data mapping & aggregates
//
export const createReport = ({
  journalSignal,

  rows,
  rowSums = {},
  rowSorter = (cells, a, b) => 0,
  rowFilter = (cells, row) => true,

  cols,
  colSums = {},
  colSorter = (cells, a, b) => 0,
  colFilter = (cells, col) => true,

  initData = (report)=>{},
  mapData = (report, line)=>{},

  isCumulative = false,
}) => {
  //
  // Fetch and organize journal signal
  //
  const {line_ids, lines: nonNaLines, na_lines, accounts} = journalSignal.value
  if (!line_ids?.length) return {}

  const lines = Object.values(nonNaLines).concat(Object.values(na_lines))

  const accountsLookup = accounts.reduce((o, a) => {
    o[a.name] = {...a}
    return o
  }, {})

  //
  // Build blank report from rows & cols
  //
  const rowIds = []
  const autoRowSums = {}
  const addRowIds = (rows) =>
    rows.forEach(({id, items, summary}) => {
      if (!items || summary) rowIds.push(id)
      if (items) addRowIds(items)
      if (summary && items.length && !rowSums[id]) autoRowSums[id] = rowSum(...items.map(({id}) => id))
    })
  addRowIds(rows)

  const colIds = []
  const autoColSums = {}
  const addColIds = (cols) =>
    cols.forEach(({id, items, summary}) => {
      if (!items || summary) colIds.push(id)
      if (items) addColIds(items)
      if (summary && items.length && !colSums[id]) autoColSums[id] = colSum(...items.map(({id}) => id))
    })
  addColIds(cols)

  const emptyRows = rowIds.reduce((data, id) => {
    data[id] = 0
    return data
  }, {})

  const cells = colIds.reduce((data, id) => {
    data[id] = {...emptyRows}
    return data
  }, {})

  //
  // Report object passed to functions and returned to view
  //
  const report = {lines, accountsLookup, rows, rowIds, cols, colIds, cells}

  //
  // Initialize report data
  //
  initData(report)

  //
  // Map each line in the journal individually first
  //
  lines.forEach((line) => mapData(report, line))

  //
  // Do cumulative calculations (like for position) if neede
  //
  if (isCumulative) {
    report.rowIds.forEach((rowId) => {
      let sum = 0
      report.colIds.forEach((colId) => {
        sum += report.cells[colId][rowId]
        report.cells[colId][rowId] = sum
      })
    })
  }

  //
  // Run aggregates on columns
  //
  Object.keys(autoColSums).forEach((colId) =>
    report.rowIds.forEach((rowId) =>
      autoColSums[colId](report, colId, rowId)))

  Object.keys(colSums).forEach((colId) =>
    report.rowIds.forEach((rowId) =>
      colSums[colId](report, colId, rowId)))

  //
  // Run aggregates on rows
  //
  Object.keys(autoRowSums).forEach((rowId) =>
    report.colIds.forEach((colId) =>
      autoRowSums[rowId](report, colId, rowId)))

  Object.keys(rowSums).forEach((rowId) =>
    report.colIds.forEach((colId) =>
      rowSums[rowId](report, colId, rowId)))

  //
  // Sort rows & cols
  //
  report.rows = applyRowColFns(rows, cells, rowSorter, rowFilter)
  report.cols = applyRowColFns(cols, cells, colSorter, colFilter)

  //
  // Pass control to the view
  //
  return report
}

export const $ = (fmt, n) => {
  const n2 = fmtLookup[fmt](n)

  return pug`
    if n2 == '-'
      .font-mono.text-sm.text-gray-800.opacity-50.pr-1= n2
    else if n < 0
      .font-mono.text-sm.text-red-800(style={marginRight: '-4px'})= n2
    else
      .font-mono.text-sm.text-gray-800.pr-1= n2
  `
}

export const $percent = (n) => {
  const n100 = Math.round(n * 100)

  return pug`
    if n100 == 0 || isNaN(n)
      .font-mono.text-sm.text-gray-700.opacity-50 -
    else if n < 0
      if n100 < -10000
        .row.items-center.font-mono.text-sm.text-red-700 -#[span.leading-none.font-light.text-3xl ∞]%
      else
        .font-mono.text-sm.text-red-700 #{n100}%
    else
      if n100 > 10000
        .row.items-center.font-mono.text-sm.text-green-700 +#[span.leading-none.font-light.text-3xl ∞]%
      else
        .font-mono.text-sm.text-green-700 +#{n100}%
  `
}

export const $change = (fmt, n) => {
  const n2 = fmtLookup[fmt](n).replaceAll(/[)(]/g, '')

  return pug`
    if n2 == '-'
      .font-mono.center.italic.text-xs.text-gray-800.opacity-50.pr-1= n2
    else if n < 0
      .font-mono.center.italic.text-xs.text-red-800.pr-1 -#{n2}
    else
      .font-mono.center.italic.text-xs.text-green-800.pr-1 +#{n2}
  `
}

//
// Render a generic report from a computed report signal
//
export const Report = ({report, fmt='kes', children}) => pug`
  // - const data = report.value

  unless report
    Loading
  else
    Ready(data=report fmt=fmt)
      = children
`

const Ready = ({data, fmt='kes', children}) => {
  const rowsRef = useRef()
  const colsRef = useRef()
  const cellsRef = useRef()
  useEffect(() => {
    const r = rowsRef.current, c = colsRef.current, x = cellsRef.current
    if (!r || !c || !x) return

    const onScrollRowsOrCols = () => {
      const left = c.scrollLeft, top = r.scrollTop
      x.scroll(left, top)
    }

    const onScrollCells = () => {
      const left = x.scrollLeft, top = x.scrollTop
      r.scroll(0, top)
      c.scroll(left, 0)
    }

    r.addEventListener('scroll', onScrollRowsOrCols)
    c.addEventListener('scroll', onScrollRowsOrCols)
    x.addEventListener('scroll', onScrollCells)

    return () => {
      r.removeEventListener('scroll', onScrollRowsOrCols)
      c.removeEventListener('scroll', onScrollRowsOrCols)
      x.removeEventListener('scroll', onScrollCells)
    }
  }, [rowsRef, colsRef, cellsRef])

  return pug`
    .col.w-full.h-full.overflow-hidden
      .row.w-full.flex-shrink-0.overflow-x-hidden.pr-2
        //
        // Settings
        //
        .w-80.min-h-full.p-2
          = children

        //
        // Cols Header
        //
        .row.flex-1.h-full.overflow-x-scroll#cols(ref=colsRef style={paddingRight: '48px'})
          for col in data.cols
            ColHeader(key=col.id col=col colLvl=1)

      .row.w-full.flex-1.overflow-hidden
        //
        // Rows Header
        //
        .col.w-80.h-full.overflow-hidden
          .col.w-full.flex-1.pl-6.overflow-y-scroll.overflow-x-hidden#rows(ref=rowsRef style={paddingBottom: '48px', paddingLeft: '-12px'})
            for row in data.rows
              RowHeader(key=row.id row=row rowLvl=1)

        //
        // Cells
        //
        .col.flex-1.overflow-scroll.pb-12.pr-14#cells(ref=cellsRef)
          for row in data.rows
            Row(key=row.id row=row rowId=row.id rowLvl=1 cols=data.cols cells=data.cells fmt=fmt)
  `
}

const ROW_CLX = {
  title: {
    1: ' h-10 items-end ',
    2: ' h-8 items-end ',
    3: ' h-8 items-end ',
    4: ' h-8 items-end ',
  },
  items: {
    1: ' ',
    2: ' ',
    3: ' ',
    4: ' ',
  },
  item: {
    3: ' h-7 items-end odd_bg-gray-100 ',
    1: ' h-7 items-end odd_bg-gray-100 ',
    2: ' h-7 items-end odd_bg-gray-100 ',
    4: ' h-7 items-end odd_bg-gray-100 ',
  },
  summary: {
    1: ' h-7 items-center bg-yellow-300 ',
    2: ' h-7 items-center bg-yellow-100 ',
    3: ' h-7 items-center ',
    4: ' h-7 items-center ',
  },
}

const COL_CLX = {
  title: {
    1: ' h-10 whitespace-nowrap ',
    2: ' h-10 whitespace-nowrap ',
    3: ' h-10 whitespace-nowrap ',
    4: ' h-10 whitespace-nowrap ',
  },
  outerContainer: {
    1: ' border-l-2 border-r-2 border-t border-gray-600 ',
    2: ' border-l-2 border-r-2 border-t border-gray-600 ',
    3: ' border-l-2 border-r-2 border-t border-gray-600 ',
    4: ' border-l-2 border-r-2 border-t border-gray-600 ',
  },
  innerContainer: {
    1: ' border-l-2 border-r-2 border-gray-600 ',
    2: ' border-l-2 border-r-2 border-gray-600 ',
    3: ' border-l-2 border-r-2 border-gray-600 ',
    4: ' border-l-2 border-r-2 border-gray-600 ',
  },
  items: {
    1: ' ',
    2: ' ',
    3: ' ',
    4: ' ',
  },
  item: {
    1: ' w-28 p-1 justify-end ',
    2: ' w-28 p-1 justify-end ',
    3: ' w-28 p-1 justify-end ',
    4: ' w-28 p-1 justify-end ',
  },
  summary: {
    1: ' w-28 p-1 justify-end bg-yellow-300 ',
    2: ' w-28 p-1 justify-end bg-yellow-100 ',
    3: ' w-28 p-1 justify-end ',
    4: ' w-28 p-1 justify-end ',
  },
}

const COL_HEADER_CLX = {
  title: {
    1: `${COL_CLX.title[1]} text-2xl font-bold `,
    2: `${COL_CLX.title[2]} text-xl font-semibold `,
    3: `${COL_CLX.title[3]} text-lg font-medium `,
    4: `${COL_CLX.title[3]} text-lg font-medium `,
  },
  items: {
    1: `${COL_CLX.items[1]} border-b border-gray-600 `,
    2: `${COL_CLX.items[2]} `,
    3: `${COL_CLX.items[3]} `,
    4: `${COL_CLX.items[3]} `,
  },
  item: {
    1: `${COL_CLX.item[1]} `,
    2: `${COL_CLX.item[2]} `,
    3: `${COL_CLX.item[3]} `,
    4: `${COL_CLX.item[3]} `,
  },
  summary: {
    1: `${COL_CLX.summary[1]} border-b border-gray-600 text-xl font-bold bg-yellow-300 `,
    2: `${COL_CLX.summary[2]} border-b border-gray-600 text-lg italic font-semibold bg-yellow-100 `,
    3: `${COL_CLX.summary[3]} border-b border-gray-600 italic font-medium `,
    4: `${COL_CLX.summary[3]} border-b border-gray-600 italic font-medium `,
  },
}

const ROW_HEADER_CLX = {
  title: {
    2: `${ROW_CLX.title[2]} border-r border-gray-600 text-xl font-semibold `,
    1: `${ROW_CLX.title[1]} border-r border-gray-600 text-2xl font-bold `,
    3: `${ROW_CLX.title[3]} border-r border-gray-600 text-lg font-medium `,
    4: `${ROW_CLX.title[3]} border-r border-gray-600 text-lg font-medium `,
  },
  items: {
    1: `${ROW_CLX.items[1]} border-l border-gray-600 `,
    2: `${ROW_CLX.items[2]} border-l border-gray-600 `,
    3: `${ROW_CLX.items[3]} border-l border-gray-600 `,
    4: `${ROW_CLX.items[3]} border-l border-gray-600 `,
  },
  item: {
    1: `${ROW_CLX.item[1]} pl-4 border-r border-gray-600 `,
    2: `${ROW_CLX.item[2]} pl-4 border-r border-gray-600 `,
    3: `${ROW_CLX.item[3]} pl-4 border-r border-gray-600 `,
    4: `${ROW_CLX.item[3]} pl-4 border-r border-gray-600 `,
  },
  summary: {
    1: `${ROW_CLX.summary[1]} border-r border-t border-gray-600 pl-2 text-lg font-bold bg-yellow-300 `,
    2: `${ROW_CLX.summary[2]} border-r border-t border-gray-600 pl-2 text-lg italic font-semibold bg-yellow-100 `,
    3: `${ROW_CLX.summary[3]} border-r border-t border-gray-600 pl-2 italic font-medium `,
    4: `${ROW_CLX.summary[3]} border-r border-t border-gray-600 pl-2 italic font-medium `,
  },
}

const SUMMARY_CLX = {
  1: ' bg-yellow-300 font-bold ',
  2: ' font-semibold italic ',
  3: ' font-medium italic ',
  4: ' italic ',
}

const ColHeader = ({col: {title, items, summary}, id, colLvl}) => pug`
  if items || summary
    // Full Container
    .col.flex-shrink-0
      // Title
      .col.flex-shrink-0(className=COL_HEADER_CLX.title[colLvl])
        for line, i in title
          .row.w-full.h-full.p-1(key=line.content className=line.clx)= line.content

      // Items & Summary Container
      .row.flex-1.flex-shrink-0
        .w-4.h-fit.flex-shrink-0.bg-gray-200
        .row.h-full.flex-1(className=COL_CLX.outerContainer[colLvl])
          // Summary
          if summary
            .col.flex-shrink-0(className=COL_HEADER_CLX.summary[colLvl])
              for line, i in summary
                .row.w-full.h-full.p-1(key=line.content className=line.clx)= line.content

          // Items Inner
          .row.flex-shrink-0(className=COL_HEADER_CLX.items[colLvl])
            if items
              for subCol in items
                ColHeader(key=subCol.id col=subCol colLvl=colLvl+1)

  else
    // Item
    .col.h-full.flex-shrink-0(className=COL_HEADER_CLX.item[colLvl])
      for title, i in title
        .row.w-full.h-full.p-1(key=title.content className=title.clx)= title.content
`

const RowHeader = ({row: {title, items, summary, href}, id, rowLvl}) => pug`
  if items || summary
    .pl-4
      // Title
      .row.w-full.flex-shrink-0.truncate(className=ROW_HEADER_CLX.title[rowLvl])= title

      // Items
      .col.w-full.flex-shrink-0.truncate(className=ROW_HEADER_CLX.items[rowLvl])
        if items
          for subRow in items
            RowHeader(key=subRow.id row=subRow rowLvl=rowLvl+1)

      // Summary
      if summary
        .row.row.w-full.flex-shrink-0.truncate(className=ROW_HEADER_CLX.summary[rowLvl])= summary

  else
    // Item
    .row.w-full.flex-shrink-0.truncate(className=ROW_HEADER_CLX.item[rowLvl])
      if href
        a.hover_underline.hover_text-blue-700(href=href)= title
      else
        = title
`

const Row = ({row: {title, items, summary, type}, rowId, rowLvl, cols, cells, fmt}) => pug`
  if items || summary
    // Title
    .row.w-fit.flex-shrink-0(className=ROW_CLX.title[rowLvl])
      for col in cols
        Col(key=col.id rowId=rowId rowLvl=10 rowType=type col=col colId=col.id colLvl=1 cells=cells isTitle fmt=fmt)

    // Items
    .col.w-fit.flex-shrink-0(className=ROW_CLX.items[rowLvl])
      if items
        for subRow in items
          Row(key=subRow.id row=subRow rowId=subRow.id rowLvl=rowLvl+1 cols=cols cells=cells fmt=fmt)

      // Summary
      if summary
        .row.w-fit.flex-shrink-0(className=ROW_CLX.summary[rowLvl])
          for col in cols
            Col(key=col.id rowId=rowId rowLvl=rowLvl rowType=type col=col colId=col.id colLvl=1 cells=cells isSummary fmt=fmt)

  else
    // Item
    .row.w-fit.flex-shrink-0(className=ROW_CLX.item[rowLvl])
      for col in cols
        Col(key=col.id rowId=rowId rowLvl=rowLvl rowType=type col=col colId=col.id colLvl=1 cells=cells fmt=fmt)
`

const min = (a, b) => a < b ? a : b

const Col = ({rowId, rowLvl, rowType, col: {title, items, summary, type}, colId, colLvl, cells, isSummary = false, isTitle = false, fmt}) => pug`
  if items || summary
    .row.h-full.flex-shrink-0
      .w-4.h-full.flex-shrink-0.bg-gray-200
      .row.h-full.flex-1.flex-shrink-0(className=COL_CLX.innerContainer[colLvl])
        // Summary
        if summary
          .row.h-full.flex-shrink-0.truncate(
            className=COL_CLX.summary[colLvl]+SUMMARY_CLX[min(rowLvl, colLvl)]+(isSummary ? ' border-t border-gray-600 ' : '')
          )
            unless isTitle
              case type || rowType
                when 'percent'
                  = $percent(cells[colId][rowId])
                when 'change'
                  = $change(fmt, cells[colId][rowId])
                default
                  = $(fmt, cells[colId][rowId])

        // Items
        .row.h-full.flex-shrink-0.truncate(
          className=COL_CLX.items[colLvl]+(isSummary ? SUMMARY_CLX[min(rowLvl, 10)] : '')
        )
          if items
            for subCol in items
              Col(key=subCol.id rowId=rowId rowLvl=rowLvl rowType=rowType col=subCol colId=subCol.id colLvl=colLvl+1 cells=cells isSummary=isSummary isTitle=isTitle fmt=fmt)

  else
    // Item
    .row.h-full.flex-shrink-0.truncate(
      className=COL_CLX.item[colLvl]+(isSummary ? ' border-t border-gray-600 '+SUMMARY_CLX[min(rowLvl, colLvl)] : '')
    )
      unless isTitle
        case type || rowType
          when 'percent'
            = $percent(cells[colId][rowId])
          when 'change'
            = $change(fmt, cells[colId][rowId])
          default
            = $(fmt, cells[colId][rowId])
`

export default Report
