import {date, usd, kes} from '@/lib'

const fmtLookup = {usd, kes}

export const Entry = ({entry, fmt='kes', children}) => pug`
  .flex.flex-col.p-2.pt-8.max-w-screen-md(id='entry-'+entry.id)
    .pl-1.font-mono.text-gray-300(style={fontSize: '10px', marginBottom: '-8px'}) entry ##{entry.id}
    .flex.items-end.p-1.border-b-2.border-gray-400
      .w-24.font-bold= date(entry.date)

      .flex-1
        span.px-1.text-lg.leading-tight(
          className=(window.location.hash == '#entry-'+entry.id ? 'bg-yellow-300' : '.even_bg-gray-100')
        )= entry.note

      .w-28.text-right.font-mono.text-sm.text-gray-400= fmtLookup[fmt](entry.dr)
      .w-28.text-right.font-mono.text-sm.text-gray-400= fmtLookup[fmt](entry.cr)

    = children
`

export const Line = ({line, fmt='kes', children}) => pug`
  .flex.p-1.even_bg-gray-100(id='line-'+line.id)
    .w-24.pl-4.font-mono.text-gray-300(style={fontSize: '10px', marginBottom: '-8px'}) line ##{line.id}

    .flex-1.flex.flex-wrap.items-center(className=(line.cr ? 'pl-28' : 'pr-28'))
      a.px-1.leading-tight.text-gray-500.hover_underline.hover_text-blue-700(href='./accounts/'+line.acc)= line.acc
      .flex= children

    .w-48.text-right.font-mono.text-sm.text-gray-700(className=(line.cr ? 'pr-0' : 'pr-28'))
      = fmtLookup[fmt](line.dr || line.cr)
`

export const $tag = (sigil, tag, className) => pug`
  if tag
    .mx-1.px-1.text-xs.font-semibold(className=className) #{sigil}#{tag}
`