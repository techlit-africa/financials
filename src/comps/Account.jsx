import {date, usd, kes} from '@/lib'

const fmtLookup = {usd, kes}

export const Account = ({account, fmt='kes', children}) => pug`
  .flex.flex-col.p-2.max-w-screen-md
    .pt-6.flex.items-end.border-b-2.border-gray-400
      .flex-1.text-3xl.font-semibold.text-gray-700= account.name
      .w-28.text-right.font-mono.text-gray-500= fmtLookup[fmt](account.bal)

    = children
`

export const Line = ({line, fmt='kes', children}) => pug`
  .flex.pt-1.even_bg-gray-100(id='line-'+line.id)
    .w-24.font-semibold.text-gray-600= date(line.date)

    .flex-1.flex.flex-wrap.items-center.text-gray-800
      a.text-gray-700.hover_underline.hover_text-blue-700(href='../journal#entry-'+line.entry_id)= line.note
      .flex.pb-1= children

    - const up = (line.nat_dr && line.dr) || (!line.nat_dr && line.cr)
    .w-28.text-right
      span.px-2.font-mono.text-gray-600(className=(up ? 'bg-green-100' : 'bg-red-100'))
        = fmtLookup[fmt](up ? (line.dr || line.cr) : -(line.dr || line.cr))

    .w-28.text-right.font-mono.text-gray-700= fmtLookup[fmt](line.bal)
`

export const $tag = (sigil, tag, className) => pug`
  if tag
    .mx-1.px-1.text-xs.font-semibold(className=className) #{sigil}#{tag}
`