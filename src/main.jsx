import { render } from 'preact'

import React from 'preact/compat'
window.React = React

import {Helmet} from 'react-helmet'

import '@/index.css'

import { Loading, Failed } from '@/comps/Suspense'
import { Routes } from '@/Routes'

const crashIfMobile = () => {
  if (window.innerWidth > 800) return

  document.getElementById('app').classList = 'center min-h-screen text-xl font-semibold'
  document.getElementById('app').innerHTML = "You need to use a bigger screen to view TechLit reports, sorry.<br><br>You can use another device or try reloading the page."
  throw window
}

const App = () => pug`
  - crashIfMobile()

  Helmet
    script(defer data-domain='financials.techlitafrica.org' src='https://plausible.io/js/script.js')

  main.flex-1.w-full.min-h-screen.flex.flex-col.overflow-scroll
    Routes
`

render(pug`App`, document.getElementById('app'))
