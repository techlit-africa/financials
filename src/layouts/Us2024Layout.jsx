import {Match} from 'preact-router/match'
import {onMount} from '@/lib'
import {$btn, $nav} from '@/comps/Macros'
import {Loading, Failed} from '@/comps/Suspense'
import {loadUs2024, us2024IsLoading, us2024Failed} from '@/journals/us2024'

export const Us2024Layout = ({root=false, children}) => pug`
  - onMount(loadUs2024)

  .col.h-screen.max-w-full.pt-2.overflow-hidden
    .row.px-2.pt-2.gap-4.items-center
      a.inline.text-2xl.hover_text-blue-700.border-b-8.border-yellow-300.hover_border-blue-700(href='/us2024')
        span.font-semibold  TechLit Africa 2024
        em.px-2 (US 501c3)

      Match(
        path='/us2024'
        children=({url})=>(url.endsWith('/us2024') || $nav('/us2024', 'Back'))
      )
      .flex-1

      .text-gray-600.text-sm All figures are in USD $

      = $btn(()=>{loadUs2024(true)}, 'Rebuild')
      = $nav('/', 'Top')

    .col.w-full.flex-1.overflow-hidden
      if us2024IsLoading.value
        Loading
      else if us2024Failed.value
        Failed(message=us2024Failed.value.message)
      else
        = children
`

export default Us2024Layout
