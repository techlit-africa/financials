import {Match} from 'preact-router/match'
import {onMount} from '@/lib'
import {$btn, $nav} from '@/comps/Macros'
import {Loading, Failed} from '@/comps/Suspense'
import {loadKe2024, ke2024IsLoading, ke2024Failed} from '@/journals/ke2024'

export const Ke2024Layout = ({root=false, children}) => pug`
  - onMount(loadKe2024)

  .col.h-screen.max-w-full.pt-2.overflow-hidden
    .row.px-2.pt-2.gap-4.items-center
      a.inline.text-2xl.hover_text-blue-700.border-b-8.border-yellow-300.hover_border-blue-700(href='/ke2024')
        span.font-semibold  TechLit Kenya 2024
        em.px-2 (Kenyan CBO)

      Match(
        path='/ke2024'
        children=({url})=>(url.endsWith('/ke2024') || $nav('/ke2024', 'Back'))
      )
      .flex-1

      .text-gray-600.text-sm All figures are in KES /=

      = $btn(()=>{loadKe2024(true)}, 'Rebuild')
      = $nav('/', 'Top')

    .col.w-full.flex-1.overflow-hidden
      if ke2024IsLoading.value
        Loading
      else if ke2024Failed.value
        Failed(message=ke2024Failed.value.message)
      else
        = children
`

export default Ke2024Layout
