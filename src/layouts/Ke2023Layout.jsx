import {Match} from 'preact-router/match'
import {onMount} from '@/lib'
import {$btn, $nav} from '@/comps/Macros'
import {Loading, Failed} from '@/comps/Suspense'
import {loadKe2023, ke2023IsLoading, ke2023Failed} from '@/journals/ke2023'

export const Ke2023Layout = ({root=false, children}) => pug`
  - onMount(loadKe2023)

  .col.h-screen.max-w-full.pt-2.overflow-hidden
    .row.px-2.pt-2.gap-4.items-center
      a.inline.text-2xl.hover_text-blue-700.border-b-8.border-yellow-300.hover_border-blue-700(href='/ke2023')
        span.font-semibold  TechLit Kenya 2023
        em.px-2 (Kenyan CBO)

      Match(
        path='/ke2023'
        children=({url})=>(url.endsWith('/ke2023') || $nav('/ke2023', 'Back'))
      )
      .flex-1

      .text-gray-600.text-sm All figures are in KES /=

      = $btn(()=>{loadKe2023(true)}, 'Rebuild')
      = $nav('/', 'Top')

    .col.w-full.flex-1.overflow-hidden
      if ke2023IsLoading.value
        Loading
      else if ke2023Failed.value
        Failed(message=ke2023Failed.value.message)
      else
        = children
`

export default Ke2023Layout
