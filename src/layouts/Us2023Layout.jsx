import {Match} from 'preact-router/match'
import {onMount} from '@/lib'
import {$btn, $nav} from '@/comps/Macros'
import {Loading, Failed} from '@/comps/Suspense'
import {loadUs2023, us2023IsLoading, us2023Failed} from '@/journals/us2023'

export const Us2023Layout = ({children}) => pug`
  - onMount(loadUs2023)

  .col.h-screen.max-w-full.pt-2.overflow-hidden
    .row.px-2.pt-2.gap-4.items-center
      a.inline.text-2xl.hover_text-blue-700.border-b-8.border-yellow-300.hover_border-blue-700(href='/us2023')
        span.font-semibold  TechLit Africa 2023
        em.px-2 (US 501c3)

      Match(
        path='/us2023'
        children=({url})=>(url.endsWith('/us2023') || $nav('/us2023', 'Back'))
      )
      .flex-1

      .text-gray-600.text-sm All figures are in USD $

      = $btn(()=>{loadUs2023(true)}, 'Rebuild')
      = $nav('/', 'Top')

    .col.w-full.flex-1.overflow-hidden
      if us2023IsLoading.value
        Loading
      else if us2023Failed.value
        Failed(message=us2023Failed.value.message)
      else
        = children
`

export default Us2023Layout
