/* eslint-env node */
module.exports = {
  extends: ['modern', 'plugin:react-pug/all', './.eslintrc-auto-import.json'],
  plugins: ['react-pug'],
  root: true,

  rules: {
    'react/prop-types': 0,

    'react-pug/no-undef': 1,
    'react-pug/prop-types': 0,
    'react-pug/quotes': 0,
    'react-pug/empty-lines': 0,
    'react-pug/no-interpolation': 1,
    'react-pug/pug-lint': [
      'warn',
      {
        maximumLineLength: 80,
        validateIndentation: 2,
        validateLineBreaks: 'LF',
        disallowTrailingSpaces: true,
        disallowAttributeTemplateString: true,
        disallowClassLiteralsBeforeIdLiterals: true,
        disallowDuplicateAttributes: true,
        disallowHtmlText: true,
        disallowLegacyMixinCall: true,
        disallowMultipleLineBreaks: true,
        disallowSpacesInsideAttributeBrackets: true,
        disallowTemplateString: 'all',
        requireClassLiteralsBeforeAttributes: true,
        requireIdLiteralsBeforeAttributes: true,
        requireSpaceAfterCodeOperator: true,
      },
    ],

    'eqeqeq': 0,
    'new-cap': 0,
    'consistent-return': 0,
    'object-shorthand': 0,
    'no-empty-function': 0,
    'arrow-parens': ['warn', 'always'],
    'no-unused-vars': [
      'off',
      {varsIgnorePattern: '^_', argsIgnorePattern: '^_'},
    ],
    'camelcase': ['warn'],
    'multiline-comment-style': ['warn', 'separate-lines'],
    'no-warning-comments': ['warn'],
    'no-param-reassign': 0,
  },

  env: {browser: true},
  parser: '@babel/eslint-parser',
}
